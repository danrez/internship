import * as yup from "yup";

export const validationsSchemaLogin = yup.object().shape({
  email: yup
    .string()
    .email("Email должен иметь общепринятый вид адреса электронной почты")
    .required("Email должен быть введен"),
  password: yup.string().required("Пароль должен быть введен"),
});

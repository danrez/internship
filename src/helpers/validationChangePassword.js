import * as yup from 'yup';

export const validationsSchemaChangePassword = yup.object().shape({
    password: yup.string()
        .required('Пароль должен быть введен')
        .matches(/^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.{8,})/,
            'Пароль должен содержать'),
    passwordConfirm: yup.string()
        .oneOf([yup.ref('password')], 'Пароли не совпадают')
});
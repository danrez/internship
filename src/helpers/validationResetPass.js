import * as yup from 'yup';

export const validationsSchemaResetPassword = yup.object().shape({
    email: yup.string()
        .email('Email должен иметь общепринятый вид адреса электронной почты')
        .required('Email должен быть введен'),
});
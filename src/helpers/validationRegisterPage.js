import * as yup from 'yup';

export const validationsSchemaRegister = yup.object().shape({
    email: yup.string()
        .email('Email должен иметь общепринятый вид адреса электронной почты')
        .required('Email должен быть введен'),
    password: yup.string()
        .required('Пароль должен быть введен')
        .matches(/^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.{8,})/,
            'Пароль должен содержать'),
    passwordConfirm: yup.string()
        .oneOf([yup.ref('password')],'Пароли не совпадают')
});
import React from 'react';
import './App.css';
import AppRouter from "./components/AppRouter/AppRouter";
import * as Sentry from "@sentry/react";


function App() {

    return (
        <div className="App">
                <AppRouter/>
        </div>
    );
}

export default Sentry.withProfiler(App);

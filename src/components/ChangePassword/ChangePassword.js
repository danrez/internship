import React, { useEffect } from "react";
import s from "./ChangePassword.module.scss";
import { useFormik } from "formik";
import { NavLink } from "react-router-dom";
import { validationsSchemaChangePassword } from "../../helpers/validationChangePassword";
import { useDispatch, useSelector } from "react-redux";
import { updatePasswordSaga } from "../../redux/changePassword/changePassword.action";
import { ToastContainer, toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
import { isResetAnswerFalse } from "../../redux/authorization/authorization.action";
import { useNavigate } from "react-router-dom";
import MainButton from "../coomon/MainButton/MainButton";

const ChangePassword = () => {
  const dispatch = useDispatch();
  const isResetAnswer = useSelector(
    (state) => state.authorization.isResetAnswer
  );
  const navigate = useNavigate();

  const formik = useFormik({
    initialValues: {
      password: "",
      passwordConfirm: "",
    },
    onSubmit: (values) => {
      dispatch(updatePasswordSaga(values.password));
    },
    validationSchema: validationsSchemaChangePassword,
  });

  useEffect(() => {
    if (isResetAnswer) {
      notify();
      dispatch(isResetAnswerFalse());
      setTimeout(navigateToLogin, 4000);
    }
  }, [isResetAnswer]);

  const navigateToLogin = () => {
    navigate("/");
  };

  const notify = () => {
    toast("Пароль обновлен");
  };

  return (
    <div className={s.change}>
      <div>
        <ToastContainer
          position="top-center"
          autoClose={2000}
          hideProgressBar={false}
          newestOnTop={false}
          closeOnClick
          rtl={false}
          pauseOnFocusLoss
          draggable
          pauseOnHover
        />
      </div>
      <div className={s.change__wrapper}>
        <p className={s.change__title}>Обновить пароль</p>
        <form onSubmit={formik.handleSubmit}>
          <div className={s.change__inputBox}>
            <label className={s.change__label} htmlFor="password">
              Пароль
            </label>
            <input
              className={s.change__input}
              id="password"
              name="password"
              type="password"
              value={formik.values.password}
              onChange={formik.handleChange}
              onBlur={formik.handleBlur}
            />
            {formik.errors.password && formik.touched.password && (
              <div className={s.change__error}>{formik.errors.password}</div>
            )}
          </div>

          <div className={s.change__inputBox}>
            <label className={s.change__label} htmlFor="passwordConfirm">
              Подтверждение пароля
            </label>
            <input
              className={s.change__input}
              id="passwordConfirm"
              name="passwordConfirm"
              type="password"
              value={formik.values.passwordConfirm}
              onChange={formik.handleChange}
              onBlur={formik.handleBlur}
            />
            {formik.errors.passwordConfirm &&
              formik.touched.passwordConfirm && (
                <div className={s.change__error}>
                  {formik.errors.passwordConfirm}
                </div>
              )}
          </div>

          <div className={s.change__regBtnBox}>
            <MainButton text={"Восстановить пароль"} isFunction={false} />
          </div>

          <div className={s.change__linksBox}>
            <NavLink className={s.change__link} to="/">
              Войти
            </NavLink>

            <NavLink className={s.change__link} to="/registration">
              Регистрация
            </NavLink>
          </div>
        </form>
      </div>
    </div>
  );
};

export default ChangePassword;

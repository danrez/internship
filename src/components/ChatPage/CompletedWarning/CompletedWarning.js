import React from "react";
import s from "./CompletedWarning.module.scss";

const CompletedWarning = () => {
  return <div className={s.warning}>Диалог завершен</div>;
};

export default CompletedWarning;

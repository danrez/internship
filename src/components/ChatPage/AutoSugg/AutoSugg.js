import React, { useEffect, useState } from "react";
import Autosuggest from "react-autosuggest";
import PubNub from "pubnub";
import s from "./AutoSugg.module.scss";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faPaperPlane } from "@fortawesome/free-solid-svg-icons";
import { faFaceGrinWide } from "@fortawesome/free-solid-svg-icons";
import Picker from "emoji-picker-react";
import moment from "moment";

import { useDispatch, useSelector } from "react-redux";
import { pubnubConfig } from "../../../configs/pubnubConfig";
import { sendMessageSaga } from "../../../redux/chatpage/chatpage.action";

const AutoSugg = ({ stateMessages, getIsTyping, getTypingId }) => {
  const linkToFolder = useSelector((state) => state.chatPage.existingFolder);
  const dispatch = useDispatch();
  const myId = useSelector((state) => state.loginPage.id);
  const currentChannel = "Default";

  let myDate = moment().format("dddd, h:mm:ss");

  const [state, setState] = useState("");
  const [isEmojiOpened, setIsEmojiOpened] = useState(false);
  const [suggestions, setSuggestions] = useState([]);

  const pubnub = new PubNub({
    publishKey: pubnubConfig.publishKey,
    subscribeKey: pubnubConfig.subscribeKey,
    uuid: myId,
  });

  pubnub.addListener({
    signal: function (s) {
      getTypingId(s.publisher);
      // console.log(s.message)
      if (s.message === "1") {
        getIsTyping(true);
      } else if (s.message === "0") {
        getIsTyping(false);
      }
    },
  });

  const offerSuggestion = (value, stateMessages) => {
    for (let item of stateMessages) {
      if (item.message.includes(value)) {
        setSuggestions([
          {
            message: item.message,
          },
        ]);
      }
    }
  };

  const submitHandelMessage = (e) => {
    e.preventDefault();

    dispatch(sendMessageSaga(state, linkToFolder, myDate, myId));
    pubnub.publish({
      message: state,
      channel: currentChannel,
    });
    setState("");
    pubnub.signal({
      message: "0",
      channel: currentChannel,
    });
  };

  const handelOpenEmoji = () => {
    setIsEmojiOpened(!isEmojiOpened);
  };

  const addEmojiToText = (event, emojiObject) => {
    let newState = `${state} ${emojiObject.emoji}`;
    setState(newState);
  };

  return (
    <form className={s.autosugg}>
      <Autosuggest
        theme={{
          container: {
            width: "100%",
          },
          suggestionsContainerOpen: {
            position: "absolute",
            top: "-50px",
            left: "10px",
            backgroundColor: "#EBEBEB",
            padding: "10px",
            borderRadius: "10px",
          },
          input: {
            backgroundColor: "#EBEBEB",
            width: "100%",
            padding: "25px 40px 25px 20px",
          },
        }}
        inputProps={{
          placeholder: "Напишите сообщение ...",
          autoComplete: "abcd",
          value: state,
          name: "message",
          onChange: (event, { newValue }) => {
            setState(newValue);
            if (state.length === 0) {
              pubnub.signal({
                message: "1",
                channel: currentChannel,
              });
            }
          },
        }}
        suggestions={suggestions}
        onSuggestionsFetchRequested={({ value }) => {
          offerSuggestion(value, stateMessages);
        }}
        onSuggestionsClearRequested={() => {
          setSuggestions([]);
        }}
        getSuggestionValue={(suggestion) => suggestion.message}
        renderSuggestion={(suggestion) => <div>{suggestion.message}</div>}
      />

      <button
        className={s.autosugg__formBtn}
        type="submit"
        onClick={submitHandelMessage}
      >
        <FontAwesomeIcon icon={faPaperPlane} />
      </button>
      <button
        type="button"
        className={s.autosugg__formEmojiBtn}
        onClick={handelOpenEmoji}
      >
        <FontAwesomeIcon icon={faFaceGrinWide} />
      </button>

      {isEmojiOpened && (
        <div className={s.autosugg__emoji}>
          <Picker onEmojiClick={addEmojiToText} />
        </div>
      )}
    </form>
  );
};

export default AutoSugg;

import React from "react";
import s from "./Messages.module.scss";
import Message from "./Message/Message";
import MessageTyping from "./MessageTyping/MessageTyping";
import CompletedWarning from "../CompletedWarning/CompletedWarning";
import PropTypes from "prop-types";

const Messages = ({ stateMessages, isTyping, typingId, isDialogCompleted }) => {
  return (
    <div className={s.Messages}>
      <div>
        {stateMessages.map((message) => {
          return <Message key={Math.random()} message={message} />;
        })}
        {isDialogCompleted ? <CompletedWarning /> : null}
      </div>
      <div>
        {isTyping ? (
          <MessageTyping stateMessages={stateMessages} typingId={typingId} />
        ) : null}
      </div>
    </div>
  );
};

Messages.propTypes = {
  stateMessages: PropTypes.array,
  isTyping: PropTypes.bool,
  typingId: PropTypes.string,
  isDialogCompleted: PropTypes.bool,
};

export default Messages;

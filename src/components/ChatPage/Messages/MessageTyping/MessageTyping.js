import React from "react";
import { useSelector } from "react-redux";
import s from "./MessageTyping.module.scss";
import gif from "./../../../../assets/typingMessage.gif";
import PropTypes from "prop-types";

const MessageTyping = ({ typingId }) => {
  const myId = useSelector((state) => state.loginPage.id);

  return (
    <div>
      {typingId === myId ? null : (
        <div className={s.message}>
          <div>
            <img
              className={s.message__avatar}
              src="https://picsum.photos/50/50"
              alt="avatar"
            />
          </div>
          <p>
            <img className={s.message__gif} src={gif} />
          </p>
        </div>
      )}
    </div>
  );
};

MessageTyping.propTypes = {
  typingId: PropTypes.string,
};

export default MessageTyping;

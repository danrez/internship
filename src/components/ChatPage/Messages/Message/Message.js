import React, { useEffect, useState } from "react";
import { useSelector } from "react-redux";
import s from "./Message.module.scss";
import PropTypes from "prop-types";
import { getStorage, ref, getDownloadURL } from "firebase/storage";

const Message = ({ message }) => {
  const myId = useSelector((state) => state.loginPage.id);

  const [isImage, setIsImage] = useState(false);
  const [isImagePhone, setIsImagePhone] = useState(false);
  const [imagePhoheLink, setImagePhoneLink] = useState("");

  useEffect(() => {
    checkImage(message.message);
    checkPhoneImage(message.photo, message.id, message.photoName);
  }, [message]);

  const checkImage = (path, photo, id, photoName) => {
    const img = new Image();
    img.src = path;
    img.onload = function () {
      setIsImage(true);
    };
    img.onerror = function () {
      //setIsImage(false)
    };
  };

  const checkPhoneImage = async (photo, id, photoName) => {
    if (photo) {
      setIsImage(true);
      setIsImagePhone(true);

      const storage = getStorage();
      const reference = ref(storage, `${id}/${photoName}.jpg`);
      await getDownloadURL(reference)
        .then((url) => {
          setImagePhoneLink(url);
        })
        .catch((error) => console.log(error));
    }
  };

  return (
    <div className={s.message}>
      <div
        className={
          message.id === myId
            ? `${s.message__myMessage}`
            : `${s.message__userMessage}`
        }
      >
        {isImage ? (
          <img
            src={isImagePhone ? imagePhoheLink : message.message}
            className={s.chat__messageImage}
          />
        ) : (
          <p className={s.message__text}>{message.message}</p>
        )}

        <div
          className={
            message.id === myId
              ? s.message__box
              : `${s.message__box} ${s.message__box_user}`
          }
        >
          <img
            className={s.message__avatar}
            src="https://picsum.photos/50/50"
            alt="avatar"
          />
          <p className={s.message__time}>{message.time}</p>
        </div>
      </div>
    </div>
  );
};

Message.propTypes = {
  message: PropTypes.object,
};

export default Message;

import React, { useEffect, useState } from "react";
import s from "./ChatPage.module.scss";
import { useDispatch, useSelector } from "react-redux";
import { getDatabase, ref, onChildAdded, query } from "firebase/database";
import { setExistingFolderSaga } from "../../redux/chatpage/chatpage.action";
import Messages from "./Messages/Messages";
import { PubNubProvider } from "pubnub-react";
import { Chat } from "@pubnub/react-chat-components";
import PubNub from "pubnub";
import { pubnubConfig } from "../../configs/pubnubConfig";
import AutoSugg from "./AutoSugg/AutoSugg";

const ChatPage = () => {
  const myId = useSelector((state) => state.loginPage.id);
  const linkToFolder = useSelector((state) => state.chatPage.existingFolder);
  const completedDialogs = useSelector(
    (state) => state.completedPage.completedDialogs
  );
  const users = useSelector((state) => state.navBar.users);

  const currentChannel = "Default";

  const dispatch = useDispatch();

  const [stateMessages, setStateTest] = useState([]);
  const [isDialogCompleted, setIsDialogCompleted] = useState(false);
  const [nameOfDialogPerson, setNameOfDialogPerson] = useState([]);

  const [isTyping, setIsTyping] = useState(false);
  const [typingId, setTypingId] = useState("");

  const pubnub = new PubNub({
    publishKey: pubnubConfig.publishKey,
    subscribeKey: pubnubConfig.subscribeKey,
    uuid: myId,
  });

  useEffect(() => {
    dispatch(setExistingFolderSaga());
  }, []);

  useEffect(() => {
    const db = getDatabase();
    const messageRef = query(ref(db, `usersMessages/${linkToFolder}`));
    let messageArray = [];
    onChildAdded(messageRef, (data) => {
      messageArray.push(data.val());
      let helpArray = [...messageArray];
      setStateTest(helpArray);
    });
    checkIsDialogCompleted(linkToFolder);
    getNameOfDialogPerson(users, linkToFolder);
    console.log("ja render");
  }, [linkToFolder]);

  const getIsTyping = (answer) => {
    setIsTyping(answer);
  };

  const getTypingId = (id) => {
    setTypingId(id);
  };

  const checkIsDialogCompleted = (linkToFolder) => {
    for (let item of completedDialogs) {
      if (item.userId === linkToFolder) {
        setIsDialogCompleted(true);
      }
    }
  };

  const getNameOfDialogPerson = (users, linkToFolder) => {
    for (let item of users) {
      if (linkToFolder === item.uid) {
        setNameOfDialogPerson(item);
      }
    }
  };

  return (
    <div className={s.chatpage}>
      <PubNubProvider client={pubnub}>
        <Chat {...{ currentChannel }}>
          <div className={s.chatpage__head}>
            <p className={s.chatpage__headName}>
              {nameOfDialogPerson.name} {nameOfDialogPerson.secondName}
            </p>
          </div>
          <div className={s.chatpage__messages}>
            <Messages
              stateMessages={stateMessages}
              isTyping={isTyping}
              typingId={typingId}
              isDialogCompleted={isDialogCompleted}
            />
          </div>
          {isDialogCompleted ? null : (
            <AutoSugg
              stateMessages={stateMessages}
              getIsTyping={getIsTyping}
              getTypingId={getTypingId}
            />
          )}
        </Chat>
      </PubNubProvider>
    </div>
  );
};

export default ChatPage;

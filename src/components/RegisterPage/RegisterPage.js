import React, { useEffect } from "react";
import s from "./RegisterPage.module.scss";
import { useFormik } from "formik";
import { validationsSchemaRegister } from "../../helpers/validationRegisterPage";
import { useDispatch, useSelector } from "react-redux";
import {
  registrationUserGoogleSaga,
  registrationUserSaga,
} from "../../redux/registerPage/registerPage.action";
import { NavLink } from "react-router-dom";
import { ToastContainer, toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
import { isResetAnswerFalse } from "../../redux/authorization/authorization.action";
import { userTokenNull } from "../../redux/loginpage/loginpage.actions";
import { nullTokenFromServer } from "../../redux/homepage/homepage.action";
import { Button } from "reactstrap";
import MainButton from "../coomon/MainButton/MainButton";

const RegisterPage = () => {
  const dispatch = useDispatch();
  const isResetAnswer = useSelector(
    (state) => state.authorization.isResetAnswer
  );

  useEffect(() => {
    dispatch(userTokenNull());
    dispatch(nullTokenFromServer());
  }, []);

  useEffect(() => {
    if (isResetAnswer) {
      notify();
      dispatch(isResetAnswerFalse());
    }
  }, [isResetAnswer]);

  const formik = useFormik({
    initialValues: {
      email: "",
      password: "",
      passwordConfirm: "",
    },
    onSubmit: (values) => {
      dispatch(registrationUserSaga(values.email, values.password));
    },
    validationSchema: validationsSchemaRegister,
  });

  const notify = () => {
    toast("Регистрация прошла успешно");
  };

  const handleRegGoogle = () => {
    dispatch(registrationUserGoogleSaga());
  };

  return (
    <div className={s.register}>
      <div>
        <ToastContainer
          position="top-center"
          autoClose={2000}
          hideProgressBar={false}
          newestOnTop={false}
          closeOnClick
          rtl={false}
          pauseOnFocusLoss
          draggable
          pauseOnHover
        />
      </div>

      <div className={s.register__wrapper}>
        <p className={s.register__title}>Регистрация</p>
        <form onSubmit={formik.handleSubmit}>
          <div className={s.register__inputBox}>
            <label className={s.register__label} htmlFor="email">
              E-mail
            </label>
            <input
              className={s.register__input}
              id="email"
              name="email"
              type="email"
              value={formik.values.email}
              onChange={formik.handleChange}
              onBlur={formik.handleBlur}
            />
            {formik.errors.email && formik.touched.email && (
              <div className={s.register__error}>{formik.errors.email}</div>
            )}
          </div>

          <div className={s.register__inputBox}>
            <label className={s.register__label} htmlFor="password">
              Пароль
            </label>
            <input
              className={s.register__input}
              id="password"
              name="password"
              type="password"
              value={formik.values.password}
              onChange={formik.handleChange}
              onBlur={formik.handleBlur}
            />
            {formik.errors.password && formik.touched.password && (
              <div className={s.register__error}>{formik.errors.password}</div>
            )}
          </div>

          <div className={s.register__inputBox}>
            <label className={s.register__label} htmlFor="passwordConfirm">
              Подтверждение пароля
            </label>

            <input
              className={s.register__input}
              id="passwordConfirm"
              name="passwordConfirm"
              type="password"
              value={formik.values.passwordConfirm}
              onChange={formik.handleChange}
              onBlur={formik.handleBlur}
            />
            {formik.errors.passwordConfirm &&
              formik.touched.passwordConfirm && (
                <div className={s.register__error}>
                  {formik.errors.passwordConfirm}
                </div>
              )}
          </div>

          <div className={s.register__regBtnBox}>
            <MainButton text={"Регистрация"} />
          </div>
        </form>

        <MainButton text={"Вход с Google"} myFunc={handleRegGoogle} />

        <div className={s.register__linksBox}>
          <NavLink className={s.register__link} to="/">
            Войти
          </NavLink>

          <NavLink className={s.register__link} to="/reset">
            Забыли пароль?
          </NavLink>
        </div>
      </div>
    </div>
  );
};

export default RegisterPage;

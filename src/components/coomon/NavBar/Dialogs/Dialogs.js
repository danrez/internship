import React, { useEffect, useState } from "react";
import FilteredDialogs from "../FilteredDialogs/FilteredDialogs";
import ActiveDialogs from "../ActiveDialogs/ActiveDialogs";
import { useDispatch, useSelector } from "react-redux";
import { IS_FIND_NAME } from "../../../../redux/navbar/navbar.const";
import { nullIsActiveDialog } from "../../../../redux/dialogpage/dialogpage.action";
import PropTypes from "prop-types";

const Dialogs = ({ handleClickDialogs }) => {
  const dispatch = useDispatch();

  const myId = useSelector((state) => state.loginPage.id);
  const { activeDialogs, isNewActiveDialog } = useSelector(
    (state) => state.dialogPage
  );
  const { users, idOfFindDialogs, nameFindDialogs, isFindDialogs } =
    useSelector((state) => state.navBar);

  const [activeDialogsFilter, setActiveDialogs] = useState([]);
  const [filteredUsersByDialogs, setfilteredUsersByDialogs] = useState([]);

  useEffect(() => {
    showActiveDialogs(activeDialogs, myId);
  }, [activeDialogs, isNewActiveDialog]);

  useEffect(() => {
    let users = filterUsersByFindDialogs(activeDialogsFilter);
    if (users !== "undefined") {
      users = [...new Set(users)];
      setfilteredUsersByDialogs(users);
    }
  }, [idOfFindDialogs, nameFindDialogs]);

  const showActiveDialogs = (activeDialogs, myId) => {
    let activeUser = [];

    for (let item of activeDialogs) {
      for (let user of users) {
        if (`${myId}${user.uid}` === item) {
          activeUser.push(user);
        }
      }
    }
    activeUser = [...new Set(activeUser)];

    setActiveDialogs(activeUser);
    dispatch(nullIsActiveDialog());
  };
  const filterUsersByFindDialogs = (filteredUsers) => {
    let filterredUsersByDialogs = [];
    if (idOfFindDialogs) {
      let index = 0;
      for (let element of filteredUsers) {
        for (let item of idOfFindDialogs) {
          if (element.uid === item) {
            filterredUsersByDialogs[index] = element;
            index++;
          }
        }
      }

      return filterredUsersByDialogs;
    } else if (nameFindDialogs) {
      let index = 0;
      for (let element of filteredUsers) {
        if (nameFindDialogs === element.name) {
          filterredUsersByDialogs[index] = element;
          index++;
        }
      }
      dispatch({ type: IS_FIND_NAME });

      return filterredUsersByDialogs;
    }
  };

  return (
    <div>
      {isFindDialogs ? (
        <FilteredDialogs
          filteredUsersByDialogs={filteredUsersByDialogs}
          handleClickDialogs={handleClickDialogs}
        />
      ) : (
        <ActiveDialogs
          filteredUsers={activeDialogsFilter}
          handleClickDialogs={handleClickDialogs}
        />
      )}
    </div>
  );
};

Dialogs.propTypes = {
  handleClickDialogs: PropTypes.func,
};

export default Dialogs;

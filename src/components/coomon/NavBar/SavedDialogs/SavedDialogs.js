import React, { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { IS_FIND_NAME } from "../../../../redux/navbar/navbar.const";
import s from "./SavedDialogs.module.scss";
import PropTypes from "prop-types";

const SavedDialogs = ({ handleClickDialogs }) => {
  const dispatch = useDispatch();

  const activeSavedDialogs = useSelector(
    (state) => state.savedPage.activeSavedDialogs
  );
  const { isFindDialogs, idOfFindDialogs, nameFindDialogs } = useSelector(
    (state) => state.navBar
  );
  const [activeUsers, setActiveUsers] = useState([]);
  const [filteredUsersByDialogs, setfilteredUsersByDialogs] = useState([]);

  useEffect(() => {
    setActiveUsers(activeSavedDialogs);
  }, [activeSavedDialogs]);

  useEffect(() => {
    let users = filterUsersByFindDialogs(activeUsers);
    if (typeof users !== "undefined") {
      setfilteredUsersByDialogs(users);
    }
  }, [idOfFindDialogs, nameFindDialogs]);

  const filterUsersByFindDialogs = (filteredUsers) => {
    let filterredUsersByDialogs = [];
    if (idOfFindDialogs) {
      let index = 0;
      for (let element of filteredUsers) {
        for (let item of idOfFindDialogs) {
          if (element.uid === item) {
            filterredUsersByDialogs[index] = element;
            index++;
          }
        }
      }

      return filterredUsersByDialogs;
    } else if (nameFindDialogs) {
      let index = 0;
      for (let element of filteredUsers) {
        if (nameFindDialogs === element.name) {
          filterredUsersByDialogs[index] = element;
          index++;
        }
      }
      dispatch({ type: IS_FIND_NAME });

      return filterredUsersByDialogs;
    }
  };

  return (
    <div>
      {isFindDialogs
        ? filteredUsersByDialogs.map((user) => {
            return (
              <div className={s.dialogs} key={user.uid}>
                <div
                  onClick={() => {
                    handleClickDialogs(user.uid);
                  }}
                >
                  <img
                    className={s.dialogs__img}
                    src="https://picsum.photos/50/"
                    alt="avatar"
                  />
                </div>
                <p className={s.dialogs__name}>
                  {user.name} {user.secondName}
                </p>
              </div>
            );
          })
        : activeUsers.map((user) => {
            return (
              <div className={s.dialogs} key={user.uid}>
                <div
                  onClick={() => {
                    handleClickDialogs(user.uid);
                  }}
                >
                  <img
                    className={s.dialogs__img}
                    src="https://picsum.photos/50/"
                    alt="avatar"
                  />
                </div>
                <p className={s.dialogs__name}>
                  {user.name} {user.secondName}
                </p>
              </div>
            );
          })}
    </div>
  );
};

SavedDialogs.propTypes = {
  handleClickDialogs: PropTypes.func,
};

export default SavedDialogs;

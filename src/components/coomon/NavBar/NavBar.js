import React, { useEffect } from "react";
import s from "./NavBar.module.scss";
import { Routes, Route, NavLink } from "react-router-dom";
import { userTokenNull } from "../../../redux/loginpage/loginpage.actions";
import { useDispatch, useSelector } from "react-redux";
import { nullTokenFromServer } from "../../../redux/homepage/homepage.action";
import {
  findDialogsSaga,
  getMessagesFoldersSaga,
  getUsersFromServerSaga,
  setNameFindDialogs,
} from "../../../redux/navbar/navbar.action";
import {
  nullExistingFolder,
  setExistingFolderSaga,
} from "../../../redux/chatpage/chatpage.action";
import debounce from "lodash.debounce";
import { getActiveDialogSaga } from "../../../redux/dialogpage/dialogpage.action";
import Dialogs from "./Dialogs/Dialogs";
import SavedDialogs from "./SavedDialogs/SavedDialogs";
import CompletedDialogs from "./CompletedDialogs/CompletedDialogs";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faClock } from "@fortawesome/free-solid-svg-icons";
import { faBoxArchive } from "@fortawesome/free-solid-svg-icons";
import { faCircleCheck } from "@fortawesome/free-solid-svg-icons";
import { faAddressBook } from "@fortawesome/free-solid-svg-icons";
import { faPowerOff } from "@fortawesome/free-solid-svg-icons";
import { faMagnifyingGlass } from "@fortawesome/free-solid-svg-icons";
import { faXmark } from "@fortawesome/free-solid-svg-icons";
import SettingUserModal from "../SettingsUserModal/SettingUserModal";
import SettingsDialogsModal from "../SettingsDialogsModal/SettingsDialogsModal";
import PropTypes from "prop-types";

const NavBar = ({ handleToggleBurger }) => {
  const dispatch = useDispatch();
  const myId = useSelector((state) => state.loginPage.id);
  const initialSettings = useSelector(
    (state) => state.settingsDialogsModal.settings
  );

  useEffect(() => {
    dispatch(getUsersFromServerSaga());
    dispatch(getMessagesFoldersSaga());
  }, []);

  useEffect(() => {
    dispatch(getActiveDialogSaga());
  }, []);

  const handleClickDialogs = (idUser) => {
    dispatch(setExistingFolderSaga(myId, idUser));
  };

  const nullTokens = () => {
    dispatch(userTokenNull());
    dispatch(nullTokenFromServer());
  };

  const handleChooseDialog = () => {
    dispatch(nullExistingFolder());
  };

  const handleFindDialogs = (event) => {
    let text = event.target.value;
    dispatch(findDialogsSaga(text, myId));
    dispatch(setNameFindDialogs(text));
  };

  const updatedHandleFindDialogs = debounce(handleFindDialogs, 200);

  const handleNullExistingFolder = () => {
    dispatch(nullExistingFolder());
  };

  return (
    <div className={s.navbar}>
      <div className={s.navbar__adaptCloseMenu}>
        <NavLink to="dialogs" className={s.navbar__subtitleAdapt}>
            Wehelp
        </NavLink>

        <button onClick={handleToggleBurger}>
          <FontAwesomeIcon icon={faXmark} className={s.navbar__adaptIcon} />
        </button>
      </div>

      <div className={s.navbar__title}>
        <NavLink to="dialogs" className={s.navbar__subtitle}>
            Wehelp
        </NavLink>

        <div className={s.navbar__titleBox}>
          <button onClick={nullTokens}>
            <FontAwesomeIcon
              icon={faPowerOff}
              className={s.navbar__titleUsersBtn}
            />
          </button>
          <button onClick={handleChooseDialog}>
            <FontAwesomeIcon
              icon={faAddressBook}
              className={s.navbar__titleUsersBtn}
            />
          </button>
          <SettingUserModal />
        </div>
      </div>

      <div className={s.navbar__findMessage}>
        <label htmlFor="find"></label>
        <input
          className={s.navbar__input}
          placeholder="Найти диалог ..."
          type="text"
          id="find"
          onChange={updatedHandleFindDialogs}
        />
        <FontAwesomeIcon
          icon={faMagnifyingGlass}
          className={s.navbar__findMessageIcon}
        />
      </div>

      <div className={s.navbar__menu}>
        <NavLink to="dialogs">
          <button
            onClick={handleNullExistingFolder}
            className={s.navbar__menuBtn}
          >
            <FontAwesomeIcon icon={faClock} className={s.navbar__iconDialogs} />
          </button>
        </NavLink>

        {initialSettings.isSavedDialogs ? (
          <NavLink to="saved">
            <button
              onClick={handleNullExistingFolder}
              className={s.navbar__menuBtn}
            >
              <FontAwesomeIcon
                icon={faBoxArchive}
                className={s.navbar__iconDialogs}
              />
            </button>
          </NavLink>
        ) : null}

        {initialSettings.isCompletedDialogs ? (
          <NavLink to="completed">
            <button
              onClick={handleNullExistingFolder}
              className={s.navbar__menuBtn}
            >
              <FontAwesomeIcon
                icon={faCircleCheck}
                className={s.navbar__iconDialogs}
              />
            </button>
          </NavLink>
        ) : null}

        <SettingsDialogsModal />
      </div>

      <div className={s.navbar__user}>
        <div>
          <Routes>
            <Route
              path="dialogs"
              element={<Dialogs handleClickDialogs={handleClickDialogs} />}
            />

            <Route
              path="saved"
              element={<SavedDialogs handleClickDialogs={handleClickDialogs} />}
            />

            <Route
              path="completed"
              element={
                <CompletedDialogs handleClickDialogs={handleClickDialogs} />
              }
            />
          </Routes>
        </div>
      </div>
    </div>
  );
};

NavBar.propTypes = {
  handleToggleBurger: PropTypes.func,
};

export default NavBar;

import React, { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { IS_FIND_NAME } from "../../../../redux/navbar/navbar.const";
import s from "./CompletedDialogs.module.scss";
import PropTypes from "prop-types";

const CompletedDialogs = ({ handleClickDialogs }) => {
  const dispatch = useDispatch();
  const activeCompletedDialogs = useSelector(
    (state) => state.completedPage.activeCompletedDialogs
  );
  const { isFindDialogs, idOfFindDialogs, nameFindDialogs } = useSelector(
    (state) => state.navBar
  );

  const [activeCompletedUsers, setActiveCompletedUsers] = useState([]);
  const [filteredUsersByDialogs, setfilteredUsersByDialogs] = useState([]);

  useEffect(() => {
    setActiveCompletedUsers(activeCompletedDialogs);
  }, [activeCompletedDialogs]);

  useEffect(() => {
    const users = filterUsersByFindDialogs(activeCompletedUsers);
    if (typeof users !== "undefined") {
      setfilteredUsersByDialogs(users);
    }
  }, [idOfFindDialogs, nameFindDialogs]);

  const filterUsersByFindDialogs = (filteredUsers) => {
    let filterredUsersByDialogs = [];
    if (idOfFindDialogs) {
      let index = 0;
      for (let element of filteredUsers) {
        for (let item of idOfFindDialogs) {
          if (element.uid === item) {
            filterredUsersByDialogs[index] = element;
            index++;
          }
        }
      }

      return filterredUsersByDialogs;
    } else if (nameFindDialogs) {
      let index = 0;
      for (let element of filteredUsers) {
        if (nameFindDialogs === element.name) {
          filterredUsersByDialogs[index] = element;
          index++;
        }
      }
      dispatch({ type: IS_FIND_NAME });

      return filterredUsersByDialogs;
    }
  };

  return (
    <div>
      {isFindDialogs
        ? filteredUsersByDialogs.map((user) => {
            return (
              <div
                onClick={() => {
                  handleClickDialogs(user.uid);
                }}
                className={s.dialogs}
                key={user.uid}
              >
                <div>
                  <img
                    className={s.dialogs__avatar}
                    src="https://picsum.photos/50/"
                    alt="avatar"
                  />
                </div>
                <p className={s.dialogs__name}>
                  {user.name} {user.secondName}
                </p>
              </div>
            );
          })
        : activeCompletedUsers.map((user) => {
            return (
              <div
                onClick={() => {
                  handleClickDialogs(user.uid);
                }}
                className={s.dialogs}
                key={user.uid}
              >
                <div>
                  <img
                    className={s.dialogs__avatar}
                    src="https://picsum.photos/50/"
                    alt="avatar"
                  />
                </div>
                <p className={s.dialogs__name}>
                  {" "}
                  {user.name} {user.secondName}
                </p>
              </div>
            );
          })}
    </div>
  );
};

CompletedDialogs.propTypes = {
  handleClickDialogs: PropTypes.func,
};

export default CompletedDialogs;

import React from "react";
import s from "../Dialogs/Dialogs.module.scss";
import { useDispatch, useSelector } from "react-redux";
import { saveDialogsToServerSaga } from "../../../../redux/navbar/navbar.action";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faStar } from "@fortawesome/free-solid-svg-icons";
import PropTypes from "prop-types";

const ActiveDialogs = ({ filteredUsers, handleClickDialogs }) => {
  const dispatch = useDispatch();
  const myId = useSelector((state) => state.loginPage.id);

  const handelSaveDialogs = (id) => {
    dispatch(saveDialogsToServerSaga(myId, id));
  };

  return filteredUsers.map((user) => {
    return (
      <div className={s.dialogs} key={user.uid}>
        <button
          className={s.dialogs__saveBtn}
          onClick={() => {
            handelSaveDialogs(user.uid);
          }}
        >
          <FontAwesomeIcon icon={faStar} />
        </button>

        <div
          className={s.dialogs__box}
          onClick={() => {
            handleClickDialogs(user.uid);
          }}
        >
          <div>
            <img
              className={s.dialogs__avatar}
              src="https://picsum.photos/50/"
              alt="avatar"
            />
          </div>
          <p className={s.dialogs__name}>{user.name}</p>
        </div>
      </div>
    );
  });
};

ActiveDialogs.propTypes = {
  filteredUsers: PropTypes.array,
  handleClickDialogs: PropTypes.func,
};

export default ActiveDialogs;

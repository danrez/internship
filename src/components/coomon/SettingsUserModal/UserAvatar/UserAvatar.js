import React, { useState } from "react";
import s from "./UserAvatar.module.scss";
import { faPen } from "@fortawesome/free-solid-svg-icons";
import { faCircleCheck } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import PropTypes from "prop-types";

const UserAvatar = ({ getUpdatedData, itemValue, itemName }) => {
  const [isEdit, setIsEdit] = useState(false);
  const [text, setText] = useState("");
  const [itemValueUpdate, setItemValueUpdate] = useState(itemValue);

  const handleChangeSetting = (value) => {
    setText(value);
    setIsEdit(true);
  };

  const handleUpdateSetting = (e) => {
    let value = e.target.value;
    setText(value);
  };

  const handleSaveChangedSetting = () => {
    let itemObject = {};
    setIsEdit(false);
    itemObject[itemName] = text;
    setItemValueUpdate(text);
    getUpdatedData(itemObject);
  };

  return (
    <div>
      <div>
        <img
          src={itemValueUpdate}
          alt="userAvatar"
          className={s.userAvatar__infoAvatar}
        />
      </div>
      {isEdit ? (
        <input
          className={s.userAvatar__input}
          onChange={handleUpdateSetting}
          type="text"
          value={text}
        />
      ) : null}

      {isEdit ? (
        <button
          onClick={() => {
            handleSaveChangedSetting();
          }}
        >
          <FontAwesomeIcon icon={faCircleCheck} />
        </button>
      ) : (
        <button
          onClick={() => {
            handleChangeSetting(itemValueUpdate);
          }}
        >
          Изменить <FontAwesomeIcon icon={faPen} />
        </button>
      )}
    </div>
  );
};

UserAvatar.propTypes = {
  getUpdatedData: PropTypes.func,
  itemValue: PropTypes.string,
  itemName: PropTypes.string,
};

export default UserAvatar;

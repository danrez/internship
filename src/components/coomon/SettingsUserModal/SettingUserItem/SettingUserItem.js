import React, { useState, useEffect } from "react";
import s from "./SettingUserItem.module.scss";
import { faPen } from "@fortawesome/free-solid-svg-icons";
import { faCircleCheck } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import PropTypes from "prop-types";

const SettingUserItem = ({ itemName, itemValue, getUpdatedData }) => {
  const [isEdit, setIsEdit] = useState(false);
  const [text, setText] = useState("");
  const [itemValueUpdate, setItemValueUpdate] = useState("");
  const [correctTitle, setCorrectTitle] = useState("");

  useEffect(() => {
    getCorrectTitle(itemName);
    setItemValueUpdate(itemValue);
  }, []);

  const handleChangeSetting = (value) => {
    setText(value);
    setIsEdit(true);
  };

  const handleUpdateSetting = (e) => {
    let value = e.target.value;
    setText(value);
  };

  const handleSaveChangedSetting = (text) => {
    let itemObject = {};
    setIsEdit(false);
    itemObject[itemName] = text;
    setItemValueUpdate(text);

    getUpdatedData(itemObject);
  };

  const getCorrectTitle = (itemName) => {
    let title = "";
    if (itemName === "email") {
      title = "Email";
    } else if (itemName === "name") {
      title = "Имя";
    } else if (itemName === "secondName") {
      title = "Фамилия";
    }
    setCorrectTitle(title);
  };

  return (
    <li className={s.item}>
      <p className={s.item__title}>{correctTitle}: </p>
      {isEdit ? (
        <input
          className={s.item__input}
          onChange={handleUpdateSetting}
          value={text}
        />
      ) : (
        <p className={s.item__value}>{itemValueUpdate}</p>
      )}

      {isEdit ? (
        <button
          onClick={() => {
            handleSaveChangedSetting(text);
          }}
        >
          <FontAwesomeIcon icon={faCircleCheck} />
        </button>
      ) : (
        <button
          onClick={() => {
            handleChangeSetting(itemValueUpdate);
          }}
        >
          <FontAwesomeIcon icon={faPen} />
        </button>
      )}
    </li>
  );
};

SettingUserItem.propTypes = {
  itemName: PropTypes.string,
  itemValue: PropTypes.string,
  getUpdatedData: PropTypes.func,
};

export default SettingUserItem;

import React, { useState, useEffect } from "react";
import Modal from "react-modal";
import { useDispatch, useSelector } from "react-redux";
import s from "./SettingUserModal.module.scss";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faEllipsisVertical } from "@fortawesome/free-solid-svg-icons";
import { faXmark } from "@fortawesome/free-solid-svg-icons";
import SettingUserItem from "./SettingUserItem/SettingUserItem";
import {
  getUsersRegistrationSaga,
  updatedUserSettingsSaga,
} from "../../../redux/settingUserModal/usermodal.action";
import UserAvatar from "./UserAvatar/UserAvatar";
import { NavLink } from "react-router-dom";
import MainButton from "../MainButton/MainButton";

Modal.setAppElement("#root");
const SettingUserModal = () => {
  const dispatch = useDispatch();

  const users = useSelector(
    (state) => state.settingsUserModal.usersRegistration
  );
  const authId = useSelector((state) => state.loginPage.id);
  const isDataSaved = useSelector(
    (state) => state.settingsUserModal.isDataSaved
  );

  const [modalIsOpen, setModalIsOpen] = useState(false);
  const [user, setUser] = useState([]);
  const [arrayUserKeys, setArrayUserKeys] = useState([]);
  const [updatedSettings, setUpdatedSettings] = useState({});

  useEffect(() => {
    dispatch(getUsersRegistrationSaga());
  }, []);

  useEffect(() => {
    getUser(users, authId);
    getUserKeys(user);
    if (isDataSaved) {
      setModalIsOpen(false);
    }
  }, [users, user, isDataSaved]);

  const toggleModalWindow = () => {
    setModalIsOpen(!modalIsOpen);
  };

  const getUser = (users, authId) => {
    for (let user of users) {
      if (authId === user.uid) {
        setUser(user);
      }
    }
  };

  const getUserKeys = (user) => {
    let keyArrays = Object.keys(user);
    let keyArraysCleaned = [];
    for (let item of keyArrays) {
      if (item !== "uid" && item !== "avatar") {
        keyArraysCleaned.push(item);
      }
    }
    setArrayUserKeys(keyArraysCleaned);
  };

  const getUpdatedData = (data) => {
    let dataArray = { ...updatedSettings, ...data };
    dispatch(updatedUserSettingsSaga(dataArray, authId));
  };

  const handleSaveChanges = () => {
    dispatch(getUsersRegistrationSaga());
  };

  return (
    <div className={s.settings}>
      <button onClick={toggleModalWindow}>
        <FontAwesomeIcon
          className={s.settings__btnOpen}
          icon={faEllipsisVertical}
        />
      </button>
      <Modal
        isOpen={modalIsOpen}
        onRequestClose={toggleModalWindow}
        className={s.Modal}
        overlayClassName={s.Overlay}
      >
        <div className={s.settings__titleBox}>
          <h2 className={s.settings__title}>Мой профиль</h2>
          <button onClick={toggleModalWindow}>
            <FontAwesomeIcon className={s.settings__btnClosed} icon={faXmark} />
          </button>
        </div>

        <div className={s.settings__info}>
          <ul>
            {arrayUserKeys.map((item) => {
              return (
                <SettingUserItem
                  itemName={item}
                  itemValue={user[item]}
                  getUpdatedData={getUpdatedData}
                  key={Math.random()}
                />
              );
            })}
          </ul>

          <div>
            <UserAvatar
              getUpdatedData={getUpdatedData}
              itemValue={user.avatar}
              itemName="avatar"
            />
          </div>
        </div>

        <NavLink className={s.settings__changePas} to="/changepassword">
          Изменить пароль
        </NavLink>

        <div className={s.settings__mainBtnBox}>
          <MainButton
            text={"Сохранить изменения"}
            isFunction={true}
            myFunc={handleSaveChanges}
          />
        </div>
      </Modal>
    </div>
  );
};

export default SettingUserModal;

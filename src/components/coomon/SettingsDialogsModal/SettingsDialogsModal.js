import React, { useState } from "react";
import Modal from "react-modal";
import s from "./SettingDialogsModal.module.scss";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faEllipsisVertical } from "@fortawesome/free-solid-svg-icons";
import { faXmark } from "@fortawesome/free-solid-svg-icons";
import { Formik, Form, Field, FieldArray } from "formik";
import { useDispatch, useSelector } from "react-redux";
import { setNewSettings } from "../../../redux/settingDialogsModal/dialogmodal.action";
import SaveSettingsButton from "../SaveSettingsButton/SaveSettingsButton";

const SettingsDialogsModal = () => {
  const initialSettings = useSelector(
    (state) => state.settingsDialogsModal.settings
  );
  const dispatch = useDispatch();

  const [modalIsOpen, setModalIsOpen] = useState(false);

  const toggleModalWindow = () => {
    setModalIsOpen(!modalIsOpen);
  };

  const initialValues = {
    settings: [initialSettings],
  };

  return (
    <div className={s.settings}>
      <button onClick={toggleModalWindow}>
        <FontAwesomeIcon
          className={s.settings__btnOpen}
          icon={faEllipsisVertical}
        />
      </button>

      <Modal
        isOpen={modalIsOpen}
        onRequestClose={toggleModalWindow}
        className={s.Modal}
        overlayClassName={s.Overlay}
      >
        <div className={s.settings__titleBox}>
          <h2 className={s.settings__title}>Настройки</h2>
          <button onClick={toggleModalWindow}>
            <FontAwesomeIcon className={s.settings__btnClosed} icon={faXmark} />
          </button>
        </div>

        <div className={s.formik}>
          <Formik
            initialValues={initialValues}
            onSubmit={(values) => {
              dispatch(setNewSettings(values.settings[0]));
              setModalIsOpen(!modalIsOpen);
            }}
          >
            {({ values }) => (
              <Form>
                <FieldArray name="settings">
                  {() => (
                    <div className={s.formik__content}>
                      {values.settings.length > 0 &&
                        values.settings.map((settings, index) => (
                          <div key={Date.now()}>
                            <div
                              className={`${s.formik__inputBox} ${s.formik__inputBox_textarea}`}
                            >
                              <label
                                className={s.formik__label}
                                htmlFor={`settings.${index}.greeting`}
                              >
                                Автоматическое приветствие:{" "}
                              </label>
                              <Field
                                className={s.formik__textarea}
                                name={`settings.${index}.greeting`}
                                placeholder="Напишите приветствие ..."
                                component="textarea"
                              />
                            </div>

                            <div className={s.formik__inputBox}>
                              <label
                                className={s.formik__label}
                                htmlFor={`settings.${index}.isSavedDialogs`}
                              >
                                Показывать сохраненные диалоги:
                              </label>
                              <Field
                                name={`settings.${index}.isSavedDialogs`}
                                type="checkbox"
                              />
                            </div>

                            <div className={s.formik__inputBox}>
                              <label
                                className={s.formik__label}
                                htmlFor={`settings.${index}.isCompletedDialogs`}
                              >
                                Показывать завершенные диалоги:
                              </label>
                              <Field
                                name={`settings.${index}.isCompletedDialogs`}
                                type="checkbox"
                              />
                            </div>
                          </div>
                        ))}
                    </div>
                  )}
                </FieldArray>
                <SaveSettingsButton />
              </Form>
            )}
          </Formik>
        </div>
      </Modal>
    </div>
  );
};

export default SettingsDialogsModal;

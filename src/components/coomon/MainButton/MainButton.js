import React from "react";
import { Button } from "reactstrap";
import s from "./MainButton.module.scss";

const MainButton = ({ text, myFunc }) => {
  return (
    <div>
      <Button
        style={{ color: "#ffffff", backgroundColor: "#007AFF" }}
        block
        className={s.btn}
        color="info"
        onClick={myFunc}
      >
        {text}
      </Button>
    </div>
  );
};

export default MainButton;

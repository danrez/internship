import React from "react";
import s from "./PageTitle.module.scss";
import PropTypes from "prop-types";

const PageTitle = ({ title }) => {
  return <div className={s.title}>{title}</div>;
};

PageTitle.propTypes = {
  title: PropTypes.string,
};

export default PageTitle;

import React from "react";
import s from "./SaveSettingsButton.module.scss";
import PropTypes from "prop-types";

const SaveSettingsButton = ({ handleClick }) => {
  return (
    <div>
      {handleClick ? (
        <button
          className={s.btn}
          type="submit"
          onClick={() => {
            handleClick();
          }}
        >
          Сохранить изменения
        </button>
      ) : (
        <button className={s.btn} type="submit">
          Сохранить изменения
        </button>
      )}
    </div>
  );
};

SaveSettingsButton.propTypes = {
  handleClick: PropTypes.func,
};

export default SaveSettingsButton;

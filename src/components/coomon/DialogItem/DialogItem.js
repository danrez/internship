import React from "react";
import s from "./DialogItem.module.scss";
import PropTypes from "prop-types";

const DialogItem = ({ user, authId, handleEnterDialog }) => {
  return (
    <div className={s.dialogItem__box} key={user.uid}>
      <div>
        <img
          className={s.dialogItem__img}
          src="https://picsum.photos/50/"
          alt="avatar"
        />
      </div>
      <p className={s.dialogItem__user}>
        {user.name} {user.secondName}
      </p>
      <button
        className={s.dialogItem__btn}
        onClick={() => handleEnterDialog(user.uid, authId)}
      >
        Войти в диалог
      </button>
    </div>
  );
};

DialogItem.propTypes = {
  user: PropTypes.object,
  authId: PropTypes.string,
  handleEnterDialog: PropTypes.func,
};
export default DialogItem;

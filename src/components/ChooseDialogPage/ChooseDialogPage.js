import React, { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import {
  saveActiveDialogSaga,
  sendFirstMessageSaga,
} from "../../redux/dialogpage/dialogpage.action";
import { setExistingFolderSaga } from "../../redux/chatpage/chatpage.action";
import s from "./ChooseDialogPage.module.scss";
import { getCompletedDialogsSaga } from "../../redux/completedDialogsPage/completedDialogs.action";
import moment from "moment";
import PageTitle from "../coomon/PageTitle/PageTitle";
import DialogItem from "../coomon/DialogItem/DialogItem";

const ChooseDialogPage = () => {
  const dispatch = useDispatch();
  const { users, messagesFolders } = useSelector((state) => state.navBar);
  const activeMessagesFolders = useSelector(
    (state) => state.dialogPage.activeDialogs
  );
  const completedMessages = useSelector(
    (state) => state.completedPage.completedDialogs
  );
  const greetingMessage = useSelector(
    (state) => state.settingsDialogsModal.settings.greeting
  );
  const authId = useSelector((state) => state.loginPage.id);
  const [activeUsers, setActiveUsers] = useState([]);
  let myDate = moment().format("dddd, h:mm:ss");

  useEffect(() => {
    dispatch(getCompletedDialogsSaga());
  }, []);

  useEffect(() => {
    const chosenPersons = getChosenPersons(
      messagesFolders,
      activeMessagesFolders
    );
    const completedPersons = getCompletedPersons(
      messagesFolders,
      completedMessages
    );
    const notChosenPersonsArray = diff(chosenPersons, messagesFolders);

    let filteredPersons = diff(notChosenPersonsArray, completedPersons);
    filteredPersons = [...new Set(filteredPersons)];

    const userRender = getUserRender(users, filteredPersons);

    setActiveUsers(userRender);
  }, [messagesFolders, activeMessagesFolders, completedMessages]);

  const handleEnterDialog = (userId, authId) => {
    dispatch(saveActiveDialogSaga(userId, authId));
    if (greetingMessage.length > 0) {
      dispatch(sendFirstMessageSaga(authId, userId, greetingMessage, myDate));
    }

    dispatch(setExistingFolderSaga(authId, userId));
  };

  const getChosenPersons = (messagesFolders, activeMessagesFolders) => {
    let myActiveMessageFolders = [];
    for (let message of messagesFolders) {
      for (let activeMessage of activeMessagesFolders) {
        if (activeMessage.includes(message)) {
          myActiveMessageFolders.push(message);
        }
      }
    }
    return myActiveMessageFolders;
  };

  const getCompletedPersons = (messagesFolders, completedMessages) => {
    let completedPersons = [];
    for (let message of messagesFolders) {
      for (let completedMessage of completedMessages) {
        if (message === completedMessage.userId) {
          completedPersons.push(message);
        }
      }
    }

    return completedPersons;
  };

  const getUserRender = (users, filteredPersons) => {
    let userRender = [];
    for (let user of users) {
      let userId = user.uid;
      for (let item of filteredPersons) {
        if (item.includes(userId) && authId !== user.uid) {
          userRender.push(user);
        }
      }
    }
    return userRender;
  };

  const diff = function (firstArray, secondArray) {
    return firstArray
      .filter((i) => !secondArray.includes(i))
      .concat(secondArray.filter((i) => !firstArray.includes(i)));
  };

  return (
    <div>
      <PageTitle title="Активные диалоги" />

      <div className={s.dialog}>
        {activeUsers.map((user) => {
          return (
            <DialogItem
              key={user.uid}
              user={user}
              authId={authId}
              handleEnterDialog={handleEnterDialog}
            />
          );
        })}
      </div>
    </div>
  );
};

export default ChooseDialogPage;

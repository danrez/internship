import React, { useEffect } from "react";
import { useFormik } from "formik";
import { validationsSchemaLogin } from "../../helpers/validation";
import { useSelector, useDispatch } from "react-redux";
import {
  authorizationApi,
  userTokenNull,
} from "../../redux/loginpage/loginpage.actions";
import s from "./LoginPage.module.scss";
import { NavLink } from "react-router-dom";
import { ToastContainer, toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
import { isResetAnswerFalse } from "../../redux/authorization/authorization.action";
import { nullTokenFromServer } from "../../redux/homepage/homepage.action";
import MainButton from "../coomon/MainButton/MainButton";

const LoginPage = () => {
  const isAuthorizationFailure = useSelector(
    (state) => state.authorization.isAuthorizationFailure
  );
  const dispatch = useDispatch();
  const isResetAnswer = useSelector(
    (state) => state.authorization.isResetAnswer
  );
  const formik = useFormik({
    initialValues: {
      email: "",
      password: "",
    },
    onSubmit: (values) => {
      dispatch(authorizationApi(values.email, values.password));
    },
    validationSchema: validationsSchemaLogin,
  });

  useEffect(() => {
    dispatch(userTokenNull());
    dispatch(nullTokenFromServer());
  }, []);

  useEffect(() => {
    if (isResetAnswer) {
      notify();
      dispatch(isResetAnswerFalse());
    }
  }, [isResetAnswer]);

  const notify = () => {
    toast("Авторизация прошла успешно");
  };

  return (
    <div className={s.loginpage}>
      <div>
        <ToastContainer
          position="top-center"
          autoClose={2000}
          hideProgressBar={false}
          newestOnTop={false}
          closeOnClick
          rtl={false}
          pauseOnFocusLoss
          draggable
          pauseOnHover
        />
      </div>

      <form className={s.loginpage__form} onSubmit={formik.handleSubmit}>
        <div className={s.loginpage__wrapper}>
          <p className={s.loginpage__title}>Авторизация</p>
          <div className={s.loginpage__inputBox}>
            <label htmlFor="email">E-mail</label>
            <input
              className={s.loginpage__input}
              id="email"
              name="email"
              type="email"
              placeholder="Введите свой email"
              onChange={formik.handleChange}
              onBlur={formik.handleBlur}
              value={formik.values.email}
            />
            {formik.errors.email && formik.touched.email && (
              <div className={s.loginpage__error}>{formik.errors.email}</div>
            )}
          </div>

          <div className={s.loginpage__inputBox}>
            <label htmlFor="password">Пароль</label>
            <input
              className={s.loginpage__input}
              id="password"
              name="password"
              type="password"
              placeholder="Введите пароль"
              onChange={formik.handleChange}
              onBlur={formik.handleBlur}
              value={formik.values.password}
            />
            {formik.errors.password && formik.touched.password && (
              <div className={s.loginpage__error}>{formik.errors.password}</div>
            )}
          </div>

          <div className={s.loginpage__btnBox}>
            <div className={s.loginpage__btnWrapper}>
              <MainButton text="Войти" />
            </div>

            <NavLink className={s.loginpage__registrLink} to="registration">
              Регистрация
            </NavLink>
          </div>

          {isAuthorizationFailure ? <div>Authorization failure</div> : null}
        </div>
      </form>
    </div>
  );
};

export default LoginPage;

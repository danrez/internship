import React, { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { setActiveCompletedDialogs } from "../../redux/completedDialogsPage/completedDialogs.action";
import s from "./CompletedDialogsPage.module.scss";
import { setExistingFolderSaga } from "../../redux/chatpage/chatpage.action";
import PageTitle from "../coomon/PageTitle/PageTitle";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faStar } from "@fortawesome/free-solid-svg-icons";

const CompletedDialogsPage = () => {
  const dispatch = useDispatch();
  const completedDialogs = useSelector(
    (state) => state.completedPage.completedDialogs
  );
  const users = useSelector((state) => state.navBar.users);
  const authId = useSelector((state) => state.loginPage.id);

  const [completedUsers, setCompletedUsers] = useState([]);

  useEffect(() => {
    const usersArray = getCompletedUsers(users, completedDialogs);
    const estimationArray = getCompletedUsersEstimation(
      users,
      completedDialogs
    );
    for (let i = 0; i < estimationArray.length; i++) {
      let itemUsersArray = usersArray[i];
      itemUsersArray.estimation = estimationArray[i];
    }
    setCompletedUsers(usersArray);
  }, [completedDialogs]);

  const getCompletedUsers = (users, completedDialogs) => {
    let usersArray = [];
    for (let user of users) {
      for (let item of completedDialogs) {
        if (item.userId === user.uid) {
          usersArray.push(user);
        }
      }
    }
    return usersArray;
  };

  const getCompletedUsersEstimation = (users, completedDialogs) => {
    let estimationArray = [];
    for (let user of users) {
      for (let item of completedDialogs) {
        if (item.userId === user.uid) {
          estimationArray.push(item.estimation);
        }
      }
    }
    return estimationArray;
  };

  const handleEnterDialog = (user) => {
    dispatch(setActiveCompletedDialogs(user));
    dispatch(setExistingFolderSaga(authId, user.uid));
  };

  const getRenderEstimation = (estimation) => {
    let estimationArray = [];
    for (let i = 0; i < estimation; i++) {
      estimationArray.push(
        <FontAwesomeIcon icon={faStar} className={s.dialog__star} key={i} />
      );
    }

    return <p className={s.dialog__starBox}>{estimationArray}</p>;
  };

  return (
    <div>
      <PageTitle title="Завершенные диалоги" />

      <div className={s.dialog}>
        {completedUsers.map((user) => {
          return (
            <div className={s.dialog__box} key={user.uid}>
              <div>
                <img
                  className={s.dialog__img}
                  src="https://picsum.photos/50/"
                  alt="avatar"
                />
              </div>
              <p className={s.dialog__user}>
                {user.name} {user.secondName}
              </p>

              {getRenderEstimation(user.estimation)}

              <button
                className={s.dialog__btn}
                onClick={() => {
                  handleEnterDialog(user);
                }}
              >
                Войти в диалог
              </button>
            </div>
          );
        })}
      </div>
    </div>
  );
};

export default CompletedDialogsPage;

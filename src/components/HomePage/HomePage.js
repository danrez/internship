import React, { useState, useEffect } from "react";
import s from "./HomePage.module.scss";
import { useDispatch, useSelector } from "react-redux";
import { useNavigate } from "react-router-dom";
import { userTokenNull } from "../../redux/loginpage/loginpage.actions";
import { tokenFromServerSaga } from "../../redux/homepage/homepage.action";
import NavBar from "../coomon/NavBar/NavBar";
import ChatPage from "../ChatPage/ChatPage";
import ChooseDialogPage from "../ChooseDialogPage/ChooseDialogPage";
import { Routes, Route } from "react-router-dom";
import SavedDialogsPage from "../SavedDialogsPage/SavedDialogsPage";
import CompletedDialogsPage from "../CompletedDialogsPage/CompletedDialogsPage";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faBars } from "@fortawesome/free-solid-svg-icons";

const HomePage = () => {
  const dispatch = useDispatch();
  const userToken = useSelector((state) => state.loginPage.userToken);
  const navigate = useNavigate();
  const userTokenFromServer = useSelector(
    (state) => state.homePage.userTokenFromServer
  );
  const linkToFolder = useSelector((state) => state.chatPage.existingFolder);

  const [isNavbar, setIsNavbar] = useState(false);

  useEffect(() => {
    dispatch(tokenFromServerSaga());
  }, []);

  useEffect(() => {
    if (userTokenFromServer === null) {
      console.log("waiting for answer");
    } else if (userTokenFromServer !== userToken) {
      navigate("/");
      dispatch(userTokenNull());
    }
  }, [userTokenFromServer]);

  const handleToggleBurger = () => {
    setIsNavbar(!isNavbar);
  };

  return (
    <div className={s.homepage}>
      {isNavbar ? null : (
        <div className={s.homepage__burger}>
          <button onClick={handleToggleBurger}>
            <FontAwesomeIcon icon={faBars} className={s.homepage__burgerIcon} />
          </button>
        </div>
      )}

      <div
        className={
          isNavbar
            ? `${s.homepage__navbar} ${s.homepage__navbar_active}`
            : `${s.homepage__navbar}`
        }
      >
        <NavBar handleToggleBurger={handleToggleBurger} />
      </div>

      <div className={s.homepage__main}>
        <Routes>
          <Route
            path="dialogs"
            element={linkToFolder ? <ChatPage /> : <ChooseDialogPage />}
          />
          <Route
            path="saved"
            element={linkToFolder ? <ChatPage /> : <SavedDialogsPage />}
          />
          <Route
            path="completed"
            element={linkToFolder ? <ChatPage /> : <CompletedDialogsPage />}
          />
        </Routes>
      </div>
    </div>
  );
};

export default HomePage;

import React, { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import {
  deleteActiveSavedDialogs,
  deleteSavedDialogsSaga,
  getSavedDialogsSaga,
  setActiveSavedDialog,
  setHandleSetDialogs,
} from "../../redux/saveddialogspage/saveddialogs.action";
import { setExistingFolderSaga } from "../../redux/chatpage/chatpage.action";
import PageTitle from "../coomon/PageTitle/PageTitle";
import s from "./SavedDialogsPage.module.scss";

const SavedDialogsPage = () => {
  const dispatch = useDispatch();
  const users = useSelector((state) => state.navBar.users);
  const savedUsers = useSelector((state) => state.savedPage.savedDialogs);
  const authId = useSelector((state) => state.loginPage.id);
  const activeSavedDialogs = useSelector(
    (state) => state.savedPage.activeSavedDialogs
  );

  const [savedUsersFilter, setSavedUsersFilter] = useState([]);

  useEffect(() => {
    dispatch(getSavedDialogsSaga());
  }, []);

  useEffect(() => {
    const filteredUsers = [];
    for (let item of savedUsers) {
      for (let elem of users) {
        if (
          item.includes(authId) &&
          elem.uid !== authId &&
          item.includes(elem.uid)
        ) {
          filteredUsers.push(elem);
        }
      }
    }
    let filteredUsersClean = [...new Set(filteredUsers)];
    setSavedUsersFilter(filteredUsersClean);
  }, [activeSavedDialogs, savedUsers]);

  const handleDeleteUser = (userId) => {
    let arrayOfUsers = [];
    dispatch(deleteSavedDialogsSaga(userId, authId));
    for (let item of savedUsers) {
      if (!item.includes(userId)) {
        arrayOfUsers.push(item);
      }
    }
    let arrayOfUsersCleared = [...new Set(arrayOfUsers)];
    dispatch(setHandleSetDialogs(arrayOfUsersCleared));
    dispatch(deleteActiveSavedDialogs(userId));
  };

  const handleEnterDialog = (user, authId) => {
    dispatch(setActiveSavedDialog(user));
    dispatch(setExistingFolderSaga(authId, user.uid));
  };

  return (
    <div>
      <PageTitle title="Сохраненные диалоги" />

      <div className={s.dialog}>
        {savedUsersFilter.map((user) => {
          return (
            <div className={s.dialog__box} key={user.uid}>
              <div>
                <img
                  className={s.dialog__img}
                  src="https://picsum.photos/50/"
                  alt="avatar"
                />
              </div>
              <p className={s.dialog__user}>
                {user.name} {user.secondName}
              </p>

              <button
                className={s.dialog__btn}
                onClick={() => {
                  handleEnterDialog(user);
                }}
              >
                Войти в диалог
              </button>
              <button
                className={s.dialog__btn}
                onClick={() => {
                  handleDeleteUser(user.uid);
                }}
              >
                {" "}
                Удалить
              </button>
            </div>
          );
        })}
      </div>
    </div>
  );
};

export default SavedDialogsPage;

import React, { useEffect } from "react";
import s from "./ResetPassword.module.scss";
import { useFormik } from "formik";
import { NavLink } from "react-router-dom";
import { validationsSchemaResetPassword } from "../../helpers/validationResetPass";
import { useDispatch, useSelector } from "react-redux";
import { resetPasswordSaga } from "../../redux/resetPassword/resetPassword.action";
import { ToastContainer, toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
import { isResetAnswerFalse } from "../../redux/authorization/authorization.action";
import MainButton from "../coomon/MainButton/MainButton";

const ResetPassword = () => {
  const dispatch = useDispatch();
  const isResetAnswer = useSelector(
    (state) => state.authorization.isResetAnswer
  );
  const formik = useFormik({
    initialValues: {
      email: "",
    },
    onSubmit: (values) => {
      dispatch(resetPasswordSaga(values.email));
    },
    validationSchema: validationsSchemaResetPassword,
  });

  useEffect(() => {
    if (isResetAnswer) {
      notify();
      dispatch(isResetAnswerFalse());
    }
  }, [isResetAnswer]);

  const notify = () => {
    toast("Ссылка отправлена на почту");
  };

  return (
    <div className={s.reset}>
      <div>
        <ToastContainer
          position="top-center"
          autoClose={5000}
          hideProgressBar={false}
          newestOnTop={false}
          closeOnClick
          rtl={false}
          pauseOnFocusLoss
          draggable
          pauseOnHover
        />
      </div>

      <div className={s.reset__wrapper}>
        <p className={s.reset__title}>Восстановление пароля</p>

        <form onSubmit={formik.handleSubmit}>
          <div className={s.reset__inputBox}>
            <label className={s.reset__label} htmlFor="email">
              E-mail
            </label>
            <input
              className={s.reset__input}
              id="email"
              name="email"
              type="email"
              value={formik.values.email}
              onChange={formik.handleChange}
              onBlur={formik.handleBlur}
            />
            {formik.errors.email && formik.touched.email && (
              <div className={s.reset__error}>{formik.errors.email}</div>
            )}
          </div>

          <div className={s.reset__resBtnBox}>
            <MainButton text={"Сбросить пароль"} isFunction={false} />
          </div>
        </form>

        <div className={s.reset__linksBox}>
          <NavLink className={s.reset__link} to="/">
            Войти
          </NavLink>

          <NavLink className={s.reset__link} to="/registration">
            Регистрация
          </NavLink>
        </div>
      </div>
    </div>
  );
};

export default ResetPassword;

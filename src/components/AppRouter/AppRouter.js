import React, { useEffect } from "react";
import { Routes, Route, useNavigate } from "react-router-dom";
import HomePage from "../HomePage/HomePage";
import LoginPage from "../LoginPage/LoginPage";
import { useSelector } from "react-redux";
import bg from "../../assets/appBG.jpg";
import s from "./AppRouter.module.scss";
import RegisterPage from "../RegisterPage/RegisterPage";
import ResetPassword from "../ResetPassword/ResetPassword";
import ChangePassword from "../ChangePassword/ChangePassword";

const AppRouter = () => {
  const userToken = useSelector((state) => state.loginPage.userToken);
  const navigate = useNavigate();

  useEffect(() => {
    if (userToken) {
      navigate("/homepage/dialogs");
    } else {
      navigate("/");
    }
  }, [userToken]);

  return (
    <div style={{ backgroundImage: `url(${bg})` }}>
      <div className={s.appRouter}>
        <Routes>
          <Route path="/homepage/*" element={<HomePage />} />
          <Route path="/" element={<LoginPage />} />
          <Route path="/registration" element={<RegisterPage />} />
          <Route path="/reset" element={<ResetPassword />} />
          <Route path="/changepassword" element={<ChangePassword />} />
        </Routes>
      </div>
    </div>
  );
};
export default AppRouter;

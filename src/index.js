import React from 'react';
import {createRoot} from 'react-dom/client';
import App from './App';
import reportWebVitals from './reportWebVitals';
import {Provider} from "react-redux";
import store, {persistor} from "./redux/index";
import {BrowserRouter} from "react-router-dom/react-router-dom.development";
import {PersistGate} from 'redux-persist/integration/react';
import * as Sentry from "@sentry/react";
import { BrowserTracing } from "@sentry/tracing";
import 'bootstrap/dist/css/bootstrap.css';
import './index.css';
Sentry.init({
    dsn: "https://eeef93e65b3647059cd4ad16034ea9f5@o1224816.ingest.sentry.io/6369989",
    integrations: [new BrowserTracing()],

    // Set tracesSampleRate to 1.0 to capture 100%
    // of transactions for performance monitoring.
    // We recommend adjusting this value in production
    tracesSampleRate: 1.0,
});

const container = document.getElementById('root');
const root = createRoot(container);
root.render(
    <BrowserRouter>
        <Provider store={store}>
            <PersistGate loading={null} persistor={persistor}>
                <App tab="home"/>
            </PersistGate>
        </Provider>
    </BrowserRouter>
);


// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();

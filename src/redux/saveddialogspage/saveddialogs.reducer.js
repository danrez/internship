import {
    DELETE_ACTIVE_SAVED_DIALOGS, SET_ACTIVE_SAVED_DIALOG, SET_HANDLE_SET_DIALOGS, SET_ID_OF_DELETED_MESSAGE,
    SET_SAVED_DIALOGS
} from "./saveddialogs.const";

let initialState = {
    savedDialogs: [],
    activeSavedDialogs: [],
    idOfDeletedMessage: ''
};

export const savedPageReducer = (state = initialState, action) => {
    switch (action.type) {
        case SET_SAVED_DIALOGS:
            return {
                ...state,
                savedDialogs: action.savedDialogs
            };

        case SET_HANDLE_SET_DIALOGS:
            return {
                ...state,
                savedDialogs: action.savedDialogs
            };

        case SET_ACTIVE_SAVED_DIALOG:
            let idArrays = [];
            let updatedState = state.activeSavedDialogs;
            if (updatedState.length === 0) {
                updatedState.push(action.user);
            } else {
                for (let item of updatedState) {
                    idArrays.push(item.uid)
                }
                if(!idArrays.includes(action.user.uid)){
                    updatedState.push(action.user);
                }

            }


            return {
                ...state,
                activeSavedDialogs: [...updatedState],
            };

        case DELETE_ACTIVE_SAVED_DIALOGS:
            let activeStateAfterDelete = state.activeSavedDialogs;
            let newArray = activeStateAfterDelete.filter((item) => {
                return item.uid !== action.userId
            })

            return {
                ...state,
                activeSavedDialogs: newArray
            };

        case SET_ID_OF_DELETED_MESSAGE:
            return {
                ...state,
                idOfDeletedMessage: action.idOfDeletedMessage
            };

        default:
            return state;

    }
};
import {
    DELETE_ACTIVE_SAVED_DIALOGS, DELETE_SAVED_DIALOGS_SAGA, GET_SAVED_DIALOGS_SAGA, SET_ACTIVE_SAVED_DIALOG,
    SET_HANDLE_SET_DIALOGS,
    SET_ID_OF_DELETED_MESSAGE,
    SET_SAVED_DIALOGS
} from "./saveddialogs.const";

export const setSavedDialogs = (savedDialogs) => ({
    type: SET_SAVED_DIALOGS,
    savedDialogs: savedDialogs,

});

export const setHandleSetDialogs = (savedDialogs) => ({
    type: SET_HANDLE_SET_DIALOGS,
    savedDialogs: savedDialogs

})


export const setActiveSavedDialog = (user) => ({
    type: SET_ACTIVE_SAVED_DIALOG,
    user: user,
});

export const deleteActiveSavedDialogs = (userId) => ({
    type: DELETE_ACTIVE_SAVED_DIALOGS,
    userId: userId
});

export const setIdOfDeletedMessage = (idOfDeletedMessage) => ({
    type: SET_ID_OF_DELETED_MESSAGE,
    idOfDeletedMessage: idOfDeletedMessage
})


export const getSavedDialogsSaga = () => ({
    type: GET_SAVED_DIALOGS_SAGA
});

export const deleteSavedDialogsSaga = (userId, authId) => ({
    type: DELETE_SAVED_DIALOGS_SAGA,
    userId: userId,
    authId: authId

})




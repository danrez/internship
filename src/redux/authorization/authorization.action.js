import {AUTHORIZATION_FAILURE, IS_RESET_ANSWER_FALSE, IS_RESET_ANSWER_TRUE} from "./authorization.const";

export const authorizationFailure = () => ({
    type: AUTHORIZATION_FAILURE
});

export const isResetAnswerTrue = () => ({
    type: IS_RESET_ANSWER_TRUE
});

export const isResetAnswerFalse = () => ({
    type: IS_RESET_ANSWER_FALSE
});
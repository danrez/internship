import {AUTHORIZATION_FAILURE, IS_RESET_ANSWER_FALSE, IS_RESET_ANSWER_TRUE} from "./authorization.const";

let initialState = {
    isAuthorizationFailure: false,
    isResetAnswer: false
};

export const authorization = (state = initialState, action) => {
    switch (action.type) {
        case AUTHORIZATION_FAILURE:
            return {
                ...state,
                isAuthorizationFailure: true
            };

        case IS_RESET_ANSWER_TRUE:
            return {
                ...state,
                isResetAnswer: true
            };

        case IS_RESET_ANSWER_FALSE:
            return {
                ...state,
                isResetAnswer: false
            };

        default:
            return state;
    }
};
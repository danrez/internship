import {NULL_TOKEN_FROM_SERVER, TOKEN_FROM_SERVER} from "./homepage.const";

let initialState = {
    userTokenFromServer: null
};

export const homePageReducer = (state = initialState, action) => {
    switch (action.type) {
        case TOKEN_FROM_SERVER:
            return {
                ...state,
                userTokenFromServer: action.userTokenFromServer
            };

        case NULL_TOKEN_FROM_SERVER:
            return {
                ...state,
                userTokenFromServer: null
            };

        default:
            return state;
    }
};


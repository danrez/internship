import {NULL_TOKEN_FROM_SERVER, TOKEN_FROM_SERVER, TOKEN_FROM_SERVER_SAGA} from "./homepage.const";


export const tokenFromServerSaga = () =>
    ({
        type: TOKEN_FROM_SERVER_SAGA
    });

export const tokenFromServer = (userTokenFromServer) => ({
    type: TOKEN_FROM_SERVER,
    userTokenFromServer: userTokenFromServer
});

export const nullTokenFromServer = ()=>({
    type: NULL_TOKEN_FROM_SERVER
});


import {
    FIND_DIALOGS_SAGA, GET_MESSAGES_FOLDERS_SAGA, GET_USERS_FROM_SERVER_SAGA, SAVE_DIALOGS_TO_SERVER_SAGA,
    SET_ID_FIND_DIALOGS, SET_MESSAGES_FOLDERS,
    SET_NAME_FIND_DIALOGS,
    SET_USERS
} from "./navbar.const";

export const getUsersFromServerSaga = () => ({
    type: GET_USERS_FROM_SERVER_SAGA,
});

export const getMessagesFoldersSaga = () => ({
    type: GET_MESSAGES_FOLDERS_SAGA
});

export const findDialogsSaga = (text, id) => ({
    type: FIND_DIALOGS_SAGA,
    text: text,
    id: id
});

export const saveDialogsToServerSaga = (myId, userId) => ({
    type: SAVE_DIALOGS_TO_SERVER_SAGA,
    myId: myId,
    userId: userId
});


export const setUsers = (users) => ({
    type: SET_USERS,
    users: users
});

export const setMessagesFolders = (messagesFolders) => ({
    type: SET_MESSAGES_FOLDERS,
    messagesFolders: messagesFolders
});

export const setIdFinddialogs = (idOfFindDialogs) => ({
    type: SET_ID_FIND_DIALOGS,
    idOfFindDialogs: idOfFindDialogs
});

export const setNameFindDialogs = (nameFindDialogs) => ({
    type: SET_NAME_FIND_DIALOGS,
    nameFindDialogs: nameFindDialogs,
})



import {
    IS_FIND_NAME, SET_ID_FIND_DIALOGS, SET_MESSAGES_FOLDERS, SET_NAME_FIND_DIALOGS,
    SET_USERS
} from "./navbar.const";

let initialState = {
    users: [],
    messagesFolders: [],
    idOfFindDialogs: null,
    nameFindDialogs: '',
    isFindDialogs: null,

};

export const navbarReducer = (state = initialState, action) => {
    switch (action.type) {
        case SET_USERS:
            return {
                ...state,
                users: action.users,
            };

        case SET_MESSAGES_FOLDERS:
            return {
                ...state,
                messagesFolders: action.messagesFolders
            };

        case SET_ID_FIND_DIALOGS:
            if (action.idOfFindDialogs.length === 0) {
                return {
                    ...state,
                    idOfFindDialogs: null,
                }
            }

            else {
                return {
                    ...state,
                    idOfFindDialogs: action.idOfFindDialogs,
                    isFindDialogs: true
                }
            }

        case SET_NAME_FIND_DIALOGS:
            if (action.nameFindDialogs === '') {
                return {
                    ...state,
                    nameFindDialogs: action.nameFindDialogs,
                    isFindDialogs: null

                };
            }

            return {
                ...state,
                nameFindDialogs: action.nameFindDialogs,

            };

        case IS_FIND_NAME:
            return{
                ...state,
                isFindDialogs: true
            };



        default:
            return state;


    }
}
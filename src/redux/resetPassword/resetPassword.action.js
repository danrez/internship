import {RESET_PASSWORD_SAGA} from "./resetPassword.const";

export const resetPasswordSaga = (email) => ({
    type: RESET_PASSWORD_SAGA,
    email: email
});






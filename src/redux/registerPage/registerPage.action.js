import {REGISTRATION_USER_GOOGLE_SAGA, REGISTRATION_USER_SAGA} from "./registerPage.const";

export const registrationUserSaga = (email, password) => ({
    type: REGISTRATION_USER_SAGA,
    email: email,
    password: password
});


export const registrationUserGoogleSaga = () => ({
    type: REGISTRATION_USER_GOOGLE_SAGA
});

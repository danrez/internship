import {
    GET_MESSAGE_FROM_SERVER_SAGA, NULL_EXISTING_FOLDER, SEND_MESSAGE_SAGA, SET_EXISTING_FOLDER,
    SET_EXISTING_FOLDER_SAGA, SET_MESSAGE_VALUE,
    SET_MESSAGES
} from "./chatpage.const";

export const getMessageFromServerSaga = (id) => ({
    type: GET_MESSAGE_FROM_SERVER_SAGA,
    id: id,
});


export const sendMessageSaga = (message, linkToFolder, time, myId) => ({
    type: SEND_MESSAGE_SAGA,
    message: message,
    linkToFolder: linkToFolder,
    time: time,
    myId: myId

});

export const setMessages = (messages) => ({
    type: SET_MESSAGES,
    messages: messages
});

export const setMessageValue = (message) => ({
    type: SET_MESSAGE_VALUE,
    message: message
});

export const setExistingFolderSaga = (myId, userId) => ({
    type: SET_EXISTING_FOLDER_SAGA,
    myId: myId,
    userId: userId

});

export const setExistingFolder = (link) => ({
    type: SET_EXISTING_FOLDER,
    existingFolder: link
})

export const nullExistingFolder = () => ({
    type: NULL_EXISTING_FOLDER
});




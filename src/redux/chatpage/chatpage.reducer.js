import {NULL_EXISTING_FOLDER, SET_EXISTING_FOLDER, SET_MESSAGE_VALUE, SET_MESSAGES} from "./chatpage.const";

let initialState = {
    messages: [],
    messageValue: null,
    existingFolder: null
};

export const chatPageReducer = (state = initialState, action) => {
    switch (action.type) {

        case SET_MESSAGES:
            return {
                ...state,
                messages: action.messages
            };

        case SET_MESSAGE_VALUE:
            return {
                ...state,
                messageValue: action.message
            };

        case SET_EXISTING_FOLDER:
            return {
                ...state,
                existingFolder: action.existingFolder
            };

        case NULL_EXISTING_FOLDER:
            return {
                ...state,
                existingFolder: null
            };


        default:
            return state;
    }
};

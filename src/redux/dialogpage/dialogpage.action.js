import {
    GET_ACTIVE_DIALOG_SAGA, NULL_IS_ACTIVE_DIALOG, SAVE_ACTIVE_DIALOG_SAGA, SEND_FIRST_MESSAGE_SAGA,
    SET_ACTIVE_DIALOG, TRUE_IS_ACTIVE_DIALOG
} from "./dialogpage.const";

export const saveActiveDialogSaga = (userId, myId) => ({
    type: SAVE_ACTIVE_DIALOG_SAGA,
    userId: userId,
    myId: myId,
});

export const getActiveDialogSaga = () => ({
    type: GET_ACTIVE_DIALOG_SAGA
});

export const sendFirstMessageSaga = (authId, userId, message, time) => ({
    type: SEND_FIRST_MESSAGE_SAGA,
    authId: authId,
    userId: userId,
    message: message,
    time: time

});


export const setActiveDialog = (activeDialogs) => ({
    type: SET_ACTIVE_DIALOG,
    activeDialogs: activeDialogs
});


export const nullIsActiveDialog = () => ({
    type: NULL_IS_ACTIVE_DIALOG,
});

export const trueIsActiveDialog = () => ({
    type: TRUE_IS_ACTIVE_DIALOG,
});




import {NULL_IS_ACTIVE_DIALOG, SET_ACTIVE_DIALOG, TRUE_IS_ACTIVE_DIALOG} from "./dialogpage.const";

let initialState = {
    activeDialogs: [],
    isNewActiveDialog: null
};

export const dialogPageReducer = (state = initialState, action) => {
    switch (action.type) {
        case SET_ACTIVE_DIALOG:
            return {
                ...state,
                activeDialogs: action.activeDialogs,

            };

        case NULL_IS_ACTIVE_DIALOG:
            return {
                ...state,
                isNewActiveDialog: null
            };

        case TRUE_IS_ACTIVE_DIALOG:
            return {
                ...state,
                isNewActiveDialog: true
            };

        default:
            return state;
    }
};

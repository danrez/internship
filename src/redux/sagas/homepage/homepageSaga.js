import {onAuthStateChanged} from "firebase/auth";
import {put} from "redux-saga/effects";
import {tokenFromServer} from "../../homepage/homepage.action";
import {auth} from '../../../configs/firebase';


function getToken() {
    return new Promise(function (resolve, reject) {
        onAuthStateChanged(auth, (user) => {
            if (user) {
                resolve(user)
            }
            else {
                reject(new Error('Error user'))
            }
        })
    })
}

async function getTokenValue() {
    try {
        const userToken = await getToken()
            .then((user) => {

                return user.accessToken;
            })
        return userToken;
    }
    catch (err) {
        alert(err)
    }
}


export function* homePageGetTokenFromServer() {
    const userToken = yield getTokenValue();
    yield put(tokenFromServer(userToken))


}
import {getDatabase, ref, onValue} from "firebase/database";
import {put} from "redux-saga/effects";
import {setMessagesFolders} from "../../navbar/navbar.action";

function getMessagesFoldersFromServer() {
    return new Promise(function (resolve, reject) {
        const db = getDatabase();
        const allMessagesFolders = ref(db, `usersMessages`);
        const arrayMessagesFolders = [];
        onValue(allMessagesFolders, (snapshot) => {
            snapshot.forEach((childSnapshot) => {
                const childData = childSnapshot.key;
                arrayMessagesFolders.push(childData)
            })
            resolve(arrayMessagesFolders)
        })
    })
}

async function getMessagesFoldersValue() {
    try {
        const messagesFolders = await getMessagesFoldersFromServer()
            .then((messagesFolders) => {
                return messagesFolders;
            });
        return messagesFolders;
    }
    catch (err) {
        alert(err)
    }
}


export function* getMessagesFolders() {
    const messagesFolders = yield getMessagesFoldersValue();
    yield put(setMessagesFolders(messagesFolders))
};


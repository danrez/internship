import {getDatabase, ref, onValue} from "firebase/database";
import {put} from "redux-saga/effects";
import {setUsers} from "../../navbar/navbar.action";

function getUsers() {
    return new Promise(function (resolve, reject) {
        const db = getDatabase();
        const allUsers = ref(db, `users`);
        const arrayUsers = [];
        onValue(allUsers, (snapshot) => {
            snapshot.forEach((childSnapshot) => {
                const childData = childSnapshot.val();
                arrayUsers.push(childData)
            })
            resolve(arrayUsers)
        })
    })
}

async function getUsersValue() {
    try {
        const users = await getUsers()
            .then((users) => {
                return users;
            })
        return users;
    }
    catch (err) {
        alert(err)
    }
}


export function* getUsersFromServer() {
    const users = yield getUsersValue();
    yield put(setUsers(users))
};
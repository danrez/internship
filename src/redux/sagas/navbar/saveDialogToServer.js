import {getDatabase, ref, set} from "firebase/database";


async function saveUserMessage(myId, userId) {
    const db = await getDatabase();
    const messageListRef = ref(db, `savedUserMessages/${myId}${userId}`);
    set(messageListRef, {
        dialog: 'saved'
    });
}


export function* saveDialogToServer(action) {
    yield saveUserMessage(action.myId, action.userId)
}
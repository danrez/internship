import { getDatabase, ref, onValue } from "firebase/database";
import { setIdFinddialogs } from "../../navbar/navbar.action";
import { put } from "redux-saga/effects";

function getMessagesFromServer() {
  return new Promise(function (resolve, reject) {
    const db = getDatabase();

    const allMessagesFolders = ref(db, `/`);

    const arrayMessagesFolders = [];

    onValue(allMessagesFolders, (snapshot) => {
      snapshot.forEach((childSnapshot) => {
        const childData = childSnapshot.val();
        arrayMessagesFolders.push(childData);
      });

      resolve(arrayMessagesFolders[5]);
    });
  });
}

async function getMessagesFromServerValue() {
  try {
    const allMessagesFolder = await getMessagesFromServer().then(
      (allMessages) => {
        return allMessages;
      }
    );
    return allMessagesFolder;
  } catch (err) {
    alert(err);
  }
}

async function getArrayOfAnswers(textUser) {
  let arrayUniqueKey = [];
  let messagesOfUsers = [];
  let filteredMessage = await getMessagesFromServerValue();

  for (let item in filteredMessage) {
    let filteredMessageElement = filteredMessage[item];

    for (let elem in filteredMessageElement) {
      let messageArray = filteredMessageElement[elem].message;

      let textUserLowerCase = textUser.toLowerCase();

      let messageToLowerCase = messageArray.toLowerCase();
      let arrayOfMessage = messageToLowerCase.split(" ");

      if (
        arrayOfMessage.includes(textUserLowerCase) &&
        textUserLowerCase.length !== 0
      ) {
        arrayUniqueKey.push(elem);
      }
    }
  }

  for (let item in filteredMessage) {
    for (let uniKey of arrayUniqueKey) {
      let filteredMessageElement = filteredMessage[item];

      if (filteredMessageElement.hasOwnProperty(uniKey)) {
        messagesOfUsers.push(item);
      }
    }
  }

  return messagesOfUsers;
}

export function* findUserSaga(action) {
  const userIdOfDialogs = yield getArrayOfAnswers(action.text);

  yield put(setIdFinddialogs(userIdOfDialogs));
}

import {getAuth, signInWithPopup, GoogleAuthProvider} from "firebase/auth"
import {put} from "redux-saga/effects";
import {authorizationSuccess} from "../../loginpage/loginpage.actions";


async function registerGoogle() {
    const auth = getAuth();
    const provider = new GoogleAuthProvider();
    try {
        const userAnswer = await signInWithPopup(auth, provider)
            .then((result) => {
                // This gives you a Google Access Token. You can use it to access the Google API.
                // The signed-in user info.
                const user = result.user;
                return user;

                // ...
            }).catch((error) => {
                // Handle Errors here.
                console.log(error)
                // ...
            });
        return userAnswer;
    } catch (error) {
        console.log(error)
    }

}


export function* regUserGoogleSaga() {
    const user = yield registerGoogle();
    yield put(authorizationSuccess(user.accessToken, user.uid));
}
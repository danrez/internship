import {getAuth, createUserWithEmailAndPassword} from "firebase/auth";
import {getDatabase, ref, set} from "firebase/database";
import {put, delay} from "redux-saga/effects";
import {authorizationSuccess} from "../../loginpage/loginpage.actions";
import {isResetAnswerTrue} from "../../authorization/authorization.action";

async function regUser(email, password) {
    const auth = getAuth();
    try {
        const userAnswer = await createUserWithEmailAndPassword(auth, email, password)
            .then((userCredential) => {
                // Signed in
                const user = userCredential.user;
                return user
            })
            .catch((error) => {

                console.log(error)
                // ..
            });
        return userAnswer;
    } catch (error) {
        alert(error)
    }
};

async function saveDataInfo(user) {
    try {
        const db = await getDatabase();
        const newUser = ref(db, `usersRegistration/${user.uid}`);
        //const newUser = push(messageListRef);
        await set(newUser, {
            avatar: "https://www.shareicon.net/data/128x128/2017/01/06/868320_people_512x512.png",
            name: "",
            secondName: "",
            uid: user.uid
        });
    }
    catch (error) {

    }
}


export function* regUserSaga(action) {
    const user = yield regUser(action.email, action.password);
    yield saveDataInfo(user);
    if (user) {
        yield put(isResetAnswerTrue())
    }
    yield  delay(4000);
    yield put(authorizationSuccess(user.accessToken, user.uid));


};
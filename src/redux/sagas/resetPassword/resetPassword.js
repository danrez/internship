import {getAuth, sendPasswordResetEmail} from "firebase/auth";
import {put} from 'redux-saga/effects'
import {isResetAnswerTrue} from "../../authorization/authorization.action";


async function reset(email) {
    try {
        const auth = getAuth();
        const resetAnswer = await sendPasswordResetEmail(auth, email)
            .then(() => {
                return true;
            })
            .catch((error) => {
                console.log(error)
                // ..
            });
        return resetAnswer;
    } catch (error) {
        console.log(error)
    }
}


export function* resetPassword(action) {
    const answer = yield reset(action.email);
    if (answer) {
        yield put(isResetAnswerTrue())
    }

}
import {getDatabase, ref, set, push} from "firebase/database";
import {put} from "redux-saga/effects";
import {setMessageValue} from "../../chatpage/chatpage.action";

async function writeUserMessage(message, linkToFolder, time, myId) {
    const db = await getDatabase();
    const messageListRef = ref(db, `usersMessages/${linkToFolder}`);
    const newMessage = push(messageListRef);
    set(newMessage, {
        message: message,
        time: time,
        id: myId
    });
}

export function* chatPageSendMessage(action) {
    yield writeUserMessage(action.message, action.linkToFolder, action.time, action.myId);
    yield put(setMessageValue(action.message))
};
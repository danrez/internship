import {getDatabase, ref, onValue} from "firebase/database";
import {setExistingFolder} from "../../chatpage/chatpage.action";
import {put} from "redux-saga/effects";

function getLink(myId = '', userId = '') {
    return new Promise(function (resolve, reject) {
        const db = getDatabase();
        const id1 = myId;
        const id2 = userId;

        const allMessagesRef = ref(db, `usersMessages`);
        onValue(allMessagesRef, (snapshot) => {
            snapshot.forEach((childSnapshot) => {
                const childData = childSnapshot.key;
                if (`${id1}` === childData) {
                    resolve(childData);
                }
                else if(`${id2}` === childData){
                    resolve(childData);
                }
                else if (`${id2}${id1}` === childData) {

                    resolve(childData);

                }

            })
        });
    })
}

async function getLinkValue(myId, userId) {
    try {
        const linkValue = await getLink(myId, userId)
            .then((link) => {
                return link;
            });
        return linkValue;
    }
    catch (err) {
        alert(err)
    }
}

export function* checkExistingFolder(action) {

    const currentLinkToDialogs = yield getLinkValue(action.myId, action.userId);

    yield put(setExistingFolder(currentLinkToDialogs));

}
import {getAuth, updatePassword} from "firebase/auth";
import {isResetAnswerTrue} from "../../authorization/authorization.action";
import {put} from 'redux-saga/effects'

async function change(password) {
    try {
        const auth = getAuth();
        const user = auth.currentUser;
        const newPassword = password;

        const answer = await updatePassword(user, newPassword)
            .then(() => {

                return true;
            }).catch((error) => {
                // An error ocurred
                // ...
            });
        return answer;
    } catch (error) {
        console.log(error)
    }
}


export function* changePassword(action) {
    yield change(action.password);
    yield put(isResetAnswerTrue())

}
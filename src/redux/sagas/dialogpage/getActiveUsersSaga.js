import {getDatabase, ref, onValue} from "firebase/database";
import {setActiveDialog} from "../../dialogpage/dialogpage.action";
import {put} from "redux-saga/effects";

function getUsersId() {
    return new Promise(function (resolve, reject) {
        const db = getDatabase();
        const allUsersId = ref(db, `activeDialog`);
        const arrayUsers = [];
        onValue(allUsersId, (snapshot) => {
            snapshot.forEach((childSnapshot) => {
                const childData = childSnapshot.key;
                arrayUsers.push(childData)
            })

            resolve(arrayUsers)
        })
    })
}


export function* getActiveUsersSaga(action) {
    const arrayUsers = yield getUsersId();
    yield put(setActiveDialog(arrayUsers))

}
import {getDatabase, ref, set, push} from "firebase/database";

async function sendAutoMessage(authId, userId, message, time) {
    const db = await getDatabase();
    const messageListRef = ref(db, `usersMessages/${userId}`);
    const newMessage = push(messageListRef);
    await set(newMessage, {
        message: message,
        time: time,
        id: authId
    });
}


export function* sendFirstMessageSaga(action) {
    yield sendAutoMessage(action.authId, action.userId, action.message, action.time);


}
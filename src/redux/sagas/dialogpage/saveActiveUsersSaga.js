import {getDatabase, ref, set} from "firebase/database";
import {put} from "redux-saga/effects";
import {trueIsActiveDialog} from "../../dialogpage/dialogpage.action";


async function saveActiveDialogs(userId, myId) {
    const db = await getDatabase();
    const dialogLink = `${myId}${userId}`;


    const dialogLinkRef = ref(db, `activeDialog/${dialogLink}`);
    await set(dialogLinkRef, {
        dialog: 'active',
        id: userId
    });
}


export function* saveActiveUsersSaga(action) {

    yield saveActiveDialogs(action.userId, action.myId);
    yield put(trueIsActiveDialog());

}
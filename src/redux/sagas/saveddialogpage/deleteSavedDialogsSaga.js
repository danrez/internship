import {getDatabase, ref, remove} from "firebase/database";
import {setIdOfDeletedMessage} from "../../saveddialogspage/saveddialogs.action";

async function deleteDialogs(userId, authId) {
    const db = await getDatabase();
    const messageListRef = ref(db, `savedUserMessages/${authId}${userId}`);
    await remove(messageListRef)
}


export function* deleteSavedDialogsSaga(action) {
    yield deleteDialogs(action.userId, action.authId);
    yield setIdOfDeletedMessage(action.userId);
}
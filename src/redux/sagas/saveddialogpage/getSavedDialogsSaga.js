import {getDatabase, ref, onValue} from "firebase/database";
import {put} from "redux-saga/effects";
import {setSavedDialogs} from "../../saveddialogspage/saveddialogs.action";

function getSavedUsers() {
    return new Promise(function (resolve, reject) {
        const db = getDatabase();
        const savedUsersId = ref(db, `savedUserMessages`);
        const arrayUsers = [];
        onValue(savedUsersId, (snapshot) => {
            snapshot.forEach((childSnapshot) => {
                const childData = childSnapshot.key;
                arrayUsers.push(childData)
            })

            resolve(arrayUsers)
        })
    })
}


export function* getSavedDialogsSaga() {
    const arrayUsers = yield getSavedUsers();
    yield put(setSavedDialogs(arrayUsers))
}
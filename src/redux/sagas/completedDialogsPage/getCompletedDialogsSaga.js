import {getDatabase, ref, onValue} from "firebase/database";
import {put} from "redux-saga/effects";
import {setCompletedDialogsFromServer} from "../../completedDialogsPage/completedDialogs.action";

function getSavedUsers() {
    return new Promise(function (resolve, reject) {
        const db = getDatabase();
        const link = ref(db, `completedMessages`);

        let completedMessagesArray = [];

        onValue(link, (snapshot) => {
            snapshot.forEach((childSnapshot) => {
                const completedMessages = {
                    userId: '',
                    estimation: ''
                };
                const childData = childSnapshot.key;
                const getEstim = childSnapshot.val();
                completedMessages.userId = childData;
                completedMessages.estimation = getEstim.estimation;
                completedMessagesArray.push(completedMessages)

            })

            resolve(completedMessagesArray)
        })
    })
}


export function* getCompletedDialogsSaga() {
    const completedDialogs = yield getSavedUsers();
    yield put(setCompletedDialogsFromServer(completedDialogs))

}
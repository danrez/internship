import {all, takeLatest} from "redux-saga/effects";
import {loginpageAuthorization} from "./loginpage/loginpageSaga";
import {AUTHORIZATION_API} from "../loginpage/loginpage.const";
import {TOKEN_FROM_SERVER_SAGA} from "../homepage/homepage.const";
import {homePageGetTokenFromServer} from "./homepage/homepageSaga";
import {SEND_MESSAGE_SAGA, SET_EXISTING_FOLDER_SAGA} from "../chatpage/chatpage.const";
import {chatPageSendMessage} from "./chatpage/chatpageSendMeSaga";
import {checkExistingFolder} from "./chatpage/checkExistingFolderSaga";
import {
    FIND_DIALOGS_SAGA, GET_MESSAGES_FOLDERS_SAGA, GET_USERS_FROM_SERVER_SAGA,
    SAVE_DIALOGS_TO_SERVER_SAGA
} from "../navbar/navbar.const";
import {getUsersFromServer} from "./navbar/getUsersSaga";
import {getMessagesFolders} from "./navbar/getMessagesFoldersSaga";
import {findUserSaga} from "./navbar/findDialogs";
import {saveDialogToServer} from "./navbar/saveDialogToServer";
import {GET_ACTIVE_DIALOG_SAGA, SAVE_ACTIVE_DIALOG_SAGA, SEND_FIRST_MESSAGE_SAGA} from "../dialogpage/dialogpage.const";
import {saveActiveUsersSaga} from "./dialogpage/saveActiveUsersSaga";
import {getActiveUsersSaga} from "./dialogpage/getActiveUsersSaga";
import {DELETE_SAVED_DIALOGS_SAGA, GET_SAVED_DIALOGS_SAGA} from "../saveddialogspage/saveddialogs.const";
import {getSavedDialogsSaga} from "./saveddialogpage/getSavedDialogsSaga";
import {deleteSavedDialogsSaga} from "./saveddialogpage/deleteSavedDialogsSaga";
import {GET_COMPLETED_DIALOGS_SAGA} from "../completedDialogsPage/completedDialogs.const";
import {getCompletedDialogsSaga} from "./completedDialogsPage/getCompletedDialogsSaga";
import {GET_USERS_REGISTRATION_SAGA, UPDATE_USER_SETTINGS_SAGA} from "../settingUserModal/usermodal.const";
import {updateUserSettingSaga} from "./settingUserModal/updateUserSettingsSaga";
import {sendFirstMessageSaga} from "./dialogpage/sendFirstMessageSaga";
import {REGISTRATION_USER_GOOGLE_SAGA, REGISTRATION_USER_SAGA} from "../registerPage/registerPage.const";
import {regUserSaga} from "./registerPage/regUserSaga";
import {regUserGoogleSaga} from "./registerPage/regUserGoogleSaga";
import {resetPassword} from "./resetPassword/resetPassword";
import {RESET_PASSWORD_SAGA} from "../resetPassword/resetPassword.const";
import {getUsersRegistration} from "./settingUserModal/getUsersRegistration";
import {UPDATE_PASSWORD_SAGA} from "../changePassword/changePassword.const";
import {changePassword} from "./changePassword/changePassword";

export function* watchClickSaga() {
    yield takeLatest(AUTHORIZATION_API, loginpageAuthorization);
    yield takeLatest(TOKEN_FROM_SERVER_SAGA, homePageGetTokenFromServer);
    yield takeLatest(SEND_MESSAGE_SAGA, chatPageSendMessage);
    yield takeLatest(SET_EXISTING_FOLDER_SAGA, checkExistingFolder);
    yield takeLatest(GET_USERS_FROM_SERVER_SAGA, getUsersFromServer);
    yield takeLatest(GET_MESSAGES_FOLDERS_SAGA, getMessagesFolders);
    yield takeLatest(FIND_DIALOGS_SAGA, findUserSaga);
    yield takeLatest(SAVE_DIALOGS_TO_SERVER_SAGA, saveDialogToServer);
    yield takeLatest(SAVE_ACTIVE_DIALOG_SAGA, saveActiveUsersSaga);
    yield takeLatest(GET_ACTIVE_DIALOG_SAGA, getActiveUsersSaga);
    yield takeLatest(GET_SAVED_DIALOGS_SAGA, getSavedDialogsSaga);
    yield takeLatest(DELETE_SAVED_DIALOGS_SAGA, deleteSavedDialogsSaga);
    yield takeLatest(GET_COMPLETED_DIALOGS_SAGA, getCompletedDialogsSaga);
    yield takeLatest(UPDATE_USER_SETTINGS_SAGA, updateUserSettingSaga);
    yield takeLatest(SEND_FIRST_MESSAGE_SAGA, sendFirstMessageSaga);
    yield takeLatest(REGISTRATION_USER_SAGA, regUserSaga);
    yield takeLatest(REGISTRATION_USER_GOOGLE_SAGA, regUserGoogleSaga);
    yield takeLatest(RESET_PASSWORD_SAGA, resetPassword);
    yield takeLatest(GET_USERS_REGISTRATION_SAGA, getUsersRegistration);
    yield takeLatest(UPDATE_PASSWORD_SAGA, changePassword);

};


export default function* rootSaga() {
    yield all([
        watchClickSaga(),
    ])
};
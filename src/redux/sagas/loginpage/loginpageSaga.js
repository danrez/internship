import {signInWithEmailAndPassword} from "firebase/auth";
import {auth} from "../../../configs/firebase";
import {put, delay} from "redux-saga/effects";
import {authorizationSuccess} from "../../loginpage/loginpage.actions";
import {authorizationFailure, isResetAnswerTrue} from "../../authorization/authorization.action";

async function signIn(auth, email, password) {
    try {
        const signInAnswer = await signInWithEmailAndPassword(auth, email, password)
            .then((userCredential) => {
                return userCredential;
            })
            .catch((error) => {
                console.log(error)
                return false;

            })
        return signInAnswer;
    }
    catch (err) {
        alert(err)
    }
}

export function* loginpageAuthorization(action) {
    const signInAnswer = yield signIn(auth, action.email, action.password);
    if (signInAnswer) {
        yield put(isResetAnswerTrue());
        yield  delay(4000);
        yield put(authorizationSuccess(signInAnswer.user.accessToken, signInAnswer.user.uid));
    } else {
        yield put(authorizationFailure())
    }
}







import {getDatabase, ref, onValue} from "firebase/database";
import {setUsersRegistration} from "../../settingUserModal/usermodal.action";
import {put} from 'redux-saga/effects';

function getUsers() {
    return new Promise(function (resolve, reject) {
        const db = getDatabase();
        const allUsers = ref(db, `usersRegistration`);
        const arrayUsers = [];
        onValue(allUsers, (snapshot) => {
            snapshot.forEach((childSnapshot) => {
                const childData = childSnapshot.val();
                arrayUsers.push(childData)
            })
            resolve(arrayUsers)
        })
    })
}

async function getUsersValue() {
    try {
        const users = await getUsers()
            .then((users) => {
                return users;
            })
        return users;
    }
    catch (err) {
        alert(err)
    }
}


export function* getUsersRegistration() {
    const users = yield getUsersValue();
    yield put(setUsersRegistration(users))
}
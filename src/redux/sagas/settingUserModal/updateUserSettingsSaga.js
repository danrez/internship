import {getDatabase, ref, update} from "firebase/database";
import {put} from "redux-saga/effects";
import {setAnswerFromServer} from "../../settingUserModal/usermodal.action";

function updateSettings(settings, authId) {
    const db = getDatabase();
    const myRef = ref(db, `usersRegistration/${authId}`);
    return new Promise((resolve, reject) => {
        update(myRef, settings)
            .then(() => {
                resolve(true)
            })
            .catch((error) => {
                reject(error)
            })
    })
}


export function* updateUserSettingSaga(action) {
    const answer = yield updateSettings(action.updatedSettings, action.authId);
    yield put(setAnswerFromServer(answer));
}
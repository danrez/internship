import {createStore, applyMiddleware, combineReducers} from 'redux';
import createSagaMiddleware from 'redux-saga';
import {loginPageReducer} from "./loginpage/loginpage.reducer";
import rootSaga from './sagas/index';
import {persistStore, persistReducer} from 'redux-persist';
import storage from 'redux-persist/lib/storage';
import {homePageReducer} from "./homepage/homepage.reducer";
import {chatPageReducer} from "./chatpage/chatpage.reducer";
import {navbarReducer} from "./navbar/navbar.reducer";
import {dialogPageReducer} from "./dialogpage/dialogpage.reducer";
import {savedPageReducer} from "./saveddialogspage/saveddialogs.reducer";
import {completedPageReducer} from "./completedDialogsPage/completedDialogs.reducer";
import {settingUserModalReducer} from "./settingUserModal/usermodal.reducer";
import {settingDialogsModalReducer} from "./settingDialogsModal/dialogmodal.reducer";
import {authorization} from "./authorization/authorization.reducer";


const sagaMiddleware = createSagaMiddleware();
let reducers = combineReducers({
    loginPage: loginPageReducer,
    homePage: homePageReducer,
    chatPage: chatPageReducer,
    navBar: navbarReducer,
    dialogPage: dialogPageReducer,
    savedPage: savedPageReducer,
    completedPage: completedPageReducer,
    settingsUserModal: settingUserModalReducer,
    settingsDialogsModal: settingDialogsModalReducer,
    authorization: authorization,

});

const persistConfig = {
    key: 'root',
    storage: storage,
    whitelist: ['loginPage', 'savedPage', 'settingsDialogsModal']

};

const persistedReducer = persistReducer(persistConfig, reducers);


const store = createStore(
    persistedReducer,
    window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__(
        applyMiddleware(sagaMiddleware))
);


export const persistor = persistStore(store);
sagaMiddleware.run(rootSaga);

export default store;


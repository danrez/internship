import {SET_NEW_SETTINGS} from "./dialogmodal.const";

export const setNewSettings = (settings) => ({
    type: SET_NEW_SETTINGS,
    settings: settings
});

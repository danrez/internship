

import {SET_NEW_SETTINGS} from "./dialogmodal.const";

const initialState = {
    settings: {
        greeting: ' ',
        isSavedDialogs: true,
        isCompletedDialogs: true
    }
};

export const settingDialogsModalReducer = (state = initialState, action) => {
    switch (action.type) {
        case SET_NEW_SETTINGS:
            return {
                ...state,
                settings: {...action.settings}
            }

        default:
            return state;
    }
}
import { AUTHORIZATION_SUCCESS, USER_TOKEN_NULL} from "./loginpage.const";

let initialState = {
    userToken: null,
    id: null
};

export const loginPageReducer = (state = initialState, action) => {
    switch (action.type) {
        case AUTHORIZATION_SUCCESS:
            return {
                ...state,
                userToken: action.userToken,
                id: action.id,
            };

        case USER_TOKEN_NULL:
            return {
                ...state,
                userToken: null
            };

        default:
            return state;
    }
};
import {AUTHORIZATION_API, AUTHORIZATION_SUCCESS, USER_TOKEN_NULL} from "./loginpage.const";

export const authorizationApi = (email, password) => ({
    type: AUTHORIZATION_API,
    email: email,
    password: password
});

export const authorizationSuccess = (userToken, id) => ({
    type: AUTHORIZATION_SUCCESS,
    userToken: userToken,
    id: id

});

export const userTokenNull = () => ({
    type: USER_TOKEN_NULL,

});
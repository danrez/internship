import {SET_ACTIVE_COMPLETED_DIALOGS, SET_COMPLETED_DIALOGS_FROM_SERVER} from "./completedDialogs.const";

let initialState = {
    completedDialogs: [],
    activeCompletedDialogs: []
};

export const completedPageReducer = (state = initialState, action) => {
    switch (action.type) {
        case SET_COMPLETED_DIALOGS_FROM_SERVER:
            return {
                ...state,
                completedDialogs: action.completedDialogs
            };

        case SET_ACTIVE_COMPLETED_DIALOGS:
            let updActiveComplDialogs = state.activeCompletedDialogs;
            updActiveComplDialogs.push(action.activeCompletedDialogs);
            return {
                ...state,
                activeCompletedDialogs: [...new Set(updActiveComplDialogs)]
            };
        default:
            return state;
    }
}
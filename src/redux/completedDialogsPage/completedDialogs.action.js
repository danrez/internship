import {
    GET_COMPLETED_DIALOGS_SAGA, SET_ACTIVE_COMPLETED_DIALOGS,
    SET_COMPLETED_DIALOGS_FROM_SERVER
} from "./completedDialogs.const";

export const getCompletedDialogsSaga = () => ({
    type: GET_COMPLETED_DIALOGS_SAGA
});

export const setCompletedDialogsFromServer = (completedDialogs) => ({
    type: SET_COMPLETED_DIALOGS_FROM_SERVER,
    completedDialogs: completedDialogs

});


export const setActiveCompletedDialogs = (activeCompletedDialogs) => ({
    type: SET_ACTIVE_COMPLETED_DIALOGS,
    activeCompletedDialogs: activeCompletedDialogs
});



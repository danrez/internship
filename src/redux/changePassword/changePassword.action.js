import {UPDATE_PASSWORD_SAGA} from "./changePassword.const";

export const updatePasswordSaga = (password) => ({
    type: UPDATE_PASSWORD_SAGA,
    password: password
});
import {
    GET_USERS_REGISTRATION_SAGA, NULL_ANSWER_FROM_SERVER, SET_ANSWER_FROM_SERVER, SET_USERS_REGISTRATION,
    UPDATE_USER_SETTINGS_SAGA
} from "./usermodal.const";

export const updatedUserSettingsSaga = (updatedSettings, authId) => ({
    type: UPDATE_USER_SETTINGS_SAGA,
    updatedSettings: updatedSettings,
    authId: authId
});

export const getUsersRegistrationSaga = () => ({
    type: GET_USERS_REGISTRATION_SAGA
});

export const setAnswerFromServer = (answer) => ({
    type: SET_ANSWER_FROM_SERVER,
    answer: answer
});

export const nullAnswerFromServer = () => ({
    type: NULL_ANSWER_FROM_SERVER
});

export const setUsersRegistration = (usersRegistration) => ({
    type: SET_USERS_REGISTRATION,
    usersRegistration: usersRegistration

});


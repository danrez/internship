import {NULL_ANSWER_FROM_SERVER, SET_ANSWER_FROM_SERVER, SET_USERS_REGISTRATION} from "./usermodal.const";

const initialState = {
    isDataSaved: false,
    usersRegistration: []
};

export const settingUserModalReducer = (state = initialState, action) => {
    switch (action.type) {
        case SET_ANSWER_FROM_SERVER:
            return {
                ...state,
                isDataSaved: action.answer
            };

        case NULL_ANSWER_FROM_SERVER:
            return {
                ...state,
                isDataSaved: null
            };

        case SET_USERS_REGISTRATION:
            return {
                ...state,
                usersRegistration: [...action.usersRegistration]
            };

        default:
            return state;
    }
};
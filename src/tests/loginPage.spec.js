import React from 'react';
import LoginPage from "../components/LoginPage/LoginPage";
import Enzyme, {shallow} from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';

Enzyme.configure({adapter: new Adapter()});


it('should render LoginPage component', () => {
    const component = shallow(<LoginPage authorizationAnswer={' '}/>);
    const wrapper = component.find('.loginpage');
    expect(wrapper.length).toBe(1);
});